//
//  MainTabViewController.swift
//  SUMAIFUL
//
//  Created by occlusion on 8/29/18.
//  Copyright © 2018 Sunny Gem Co., Ltd. All rights reserved.
//

import UIKit

class TestTransition : UIPercentDrivenInteractiveTransition {
    var direction = UISwipeGestureRecognizer.Direction.left
    var enabled = false
    var initialPoint = CGPoint.zero
    func handlePanGestureRecognizer(recognizer: UIPanGestureRecognizer){
        guard enabled else{return}
        guard let view = recognizer.view else{return}
        let location = recognizer.location(in: view)
        
        if initialPoint == .zero{
            initialPoint = location
        }
        
        var per = (location.x-initialPoint.x) / view.bounds.width

        if direction == .left{
            per = -per
        }
        
        per = per * 1.5

        switch recognizer.state {
        case .began:
            break
        case .changed:
            update(per)
        case .ended:
            didEnd(recognizer: recognizer)
            break
        case .cancelled:
            didEnd(recognizer: recognizer)
            break
            
        default:
            break
        }
    }
    
    
    func didEnd(recognizer: UIPanGestureRecognizer){
        guard let view = recognizer.view else{return}
        initialPoint = .zero
        if recognizer.velocity(in: view).x > 0 && direction == .left{
            cancel()
            enabled = false
        }
        else if recognizer.velocity(in: view).x < 0 && direction == .right{
            cancel()
            enabled = false
        }
        else{
            finish()
            enabled = false
        }
    }
}

class TestAnimation : NSObject, UIViewControllerAnimatedTransitioning {
    var isRunning = false
    var direction = UISwipeGestureRecognizer.Direction.left
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.2
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        self.isRunning = true
        guard
            let from = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from),
            let to = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)
            else{return}
        let container = transitionContext.containerView

        let left = direction == .left
        let toView = !left ? from.view : to.view
        let fromView = !left ? to.view : from.view
        let offset = container.frame.width
        
        container.insertSubview(to.view, aboveSubview:from.view)

        if left{
            container.insertSubview(to.view, aboveSubview:from.view)
        }
        else{
            container.insertSubview(to.view, belowSubview:from.view)
        }
        
        toView?.frame = container.frame
        toView?.transform = left ? CGAffineTransform(translationX: offset, y: 0) : CGAffineTransform(translationX: 0, y: 0)
        
        fromView?.frame = container.frame
        fromView?.transform = left ? CGAffineTransform(translationX: 0, y: 0) : CGAffineTransform(translationX: -offset, y: 0)
        
        let duration = transitionDuration(using: transitionContext)
        UIView.animate(withDuration: duration, animations: {
            toView?.transform = left ? CGAffineTransform(translationX: 0, y: 0) : CGAffineTransform(translationX: offset, y: 0)
            fromView?.transform = left ? CGAffineTransform(translationX: -offset, y: 0) : CGAffineTransform(translationX: 0, y: 0)
        }) { (finished) in
            self.isRunning = false
            toView?.transform = CGAffineTransform.identity
            fromView?.transform = CGAffineTransform.identity
            
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        }

        
        
    }
}


class MainTabViewController: UITabBarController, UITabBarControllerDelegate, UIGestureRecognizerDelegate{
    var transition = TestTransition()
    var animation = TestAnimation()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.delegate = self
        // Do any additional setup after loading the view.
        
        if var vc = self.viewControllers{
            if Client.shared.myUser.type == .review{
                if let idx = vc.firstIndex(where: { (vc) -> Bool in
                    return vc is HomeKitViewController
                }){
                    vc.remove(at: idx)    //Home Kit
                    self.setViewControllers(vc, animated: false)
                }
            }
        }
        let pan = UIPanGestureRecognizer(target: self, action: #selector(pan(sender:)))
        pan.delegate = self
        self.view.addGestureRecognizer(pan)
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(swipeLeft(sender:)))
        swipeLeft.direction = .left
        self.view.addGestureRecognizer(swipeLeft)
        
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(swipeRight(sender:)))
        swipeRight.direction = .right
        self.view.addGestureRecognizer(swipeRight)

    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    @objc func pan(sender:UIPanGestureRecognizer) {
        transition.handlePanGestureRecognizer(recognizer: sender)
    }

    @objc func swipeLeft(sender:UISwipeGestureRecognizer) {
//        guard !animation.isRunning else{return}
        if self.selectedIndex < (self.viewControllers?.count ?? 0) - 1{
            transition.direction = sender.direction
            animation.direction = sender.direction
            transition.enabled = true

            
            self.selectedIndex = self.selectedIndex+1
            
        }
    }

    @objc func swipeRight(sender:UISwipeGestureRecognizer) {
        guard !animation.isRunning else{return}
        if self.selectedIndex > 0{
            transition.direction = sender.direction
            animation.direction = sender.direction
            transition.enabled = true
            
            
            self.selectedIndex = self.selectedIndex-1
        }
    }
    

    func tabBarController(_ tabBarController: UITabBarController, animationControllerForTransitionFrom fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        if transition.enabled{
            return animation
        }
        return nil
    }
    
    func tabBarController(_ tabBarController: UITabBarController, interactionControllerFor animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        if transition.enabled{
            return transition
        }
        return nil
    }
    
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        if let base = viewController as? TabBaseViewController{
            base.goBackToRoot()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
