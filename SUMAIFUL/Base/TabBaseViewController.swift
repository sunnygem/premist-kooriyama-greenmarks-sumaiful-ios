//
//  TabBaseViewController.swift
//  SUMAIFUL
//
//  Created by occlusion on 8/19/18.
//  Copyright © 2018 Sunny Gem Co., Ltd. All rights reserved.
//

import UIKit

class TabBaseViewController: UIViewController {

    @IBOutlet weak var rootView: UIView!
    @IBOutlet var sideMenuButton: UIImageView!
    var sideMenuRoot: UINavigationController!
    var sideMenu: SideMenuViewController!
    
    @IBOutlet weak var logoImageZeroHeightConstraint: NSLayoutConstraint?
    @IBOutlet weak var logoImageView: UIImageView!
    
    var currentViewController: UIViewController?{
        return viewControllers.last
    }
    var viewControllers =  [UIViewController]()

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup(){
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.view.superview?.backgroundColor = UIColor.white
    }
    
    
    @objc func showMenu(sender:Any){
        
        if let cv = currentViewController as? BaseViewController{
            cv.willShowMenu()
        }
        if sideMenu == nil{
            sideMenu = self.tabBarController?.storyboard?.instantiateViewController(withIdentifier: "SideMenu") as? SideMenuViewController
            sideMenuRoot = UINavigationController(rootViewController: sideMenu)
            sideMenuRoot.isNavigationBarHidden = true
        }
        
        
        
        let transition = CATransition()
        transition.type = CATransitionType.moveIn
        transition.subtype = CATransitionSubtype.fromLeft
        transition.fillMode = CAMediaTimingFillMode.both
        
        self.view.addSubview(sideMenuRoot.view)
        self.addChild(sideMenuRoot)
        sideMenuRoot.view.addFitToSuperViewSideLayout()
        sideMenuRoot.view.addFitToSuperViewTopLayout()
        sideMenuRoot.view.addFitToSuperViewBottomLayout()
        
        sideMenu.closeCallback = {[unowned self] in
            
            let transition = CATransition()
            transition.type = CATransitionType.reveal
            transition.subtype = CATransitionSubtype.fromRight

            self.sideMenuRoot.removeFromParent()
            self.sideMenuRoot.view.cleanupLayouts()
            self.sideMenuRoot.view.removeFromSuperview()
            self.view.layer.add(transition, forKey: "transition")
        }
        
        self.view.layer.add(transition, forKey: "transition")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        sideMenuButton.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(showMenu(sender:)))
        sideMenuButton.addGestureRecognizer(tap)

        view.clipsToBounds = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func goBack(){
        if viewControllers.count > 1{

            if let current = currentViewController{
                current.view.cleanupLayouts()
                current.view.removeFromSuperview()
                current.removeFromParent()
            }
            viewControllers.removeLast()

            if let vc = viewControllers.last{
                
                didChangeCurrentView(vc: vc as? BaseViewController)
                UIView.transition(with: self.rootView, duration: 0.3, options: [.transitionCrossDissolve, .curveEaseInOut], animations: {
                    self.rootView.addSubview(vc.view)
                    vc.view.addFullScreenLayout()
                    self.addChild(vc)
                }, completion: nil)

            }

        }
    }
    
    func goBackToRoot(){
        if viewControllers.count > 1{
            
            if let current = currentViewController{
                current.view.cleanupLayouts()
                current.view.removeFromSuperview()
                current.removeFromParent()
            }
            
            if let vc = viewControllers.first{
                
                didChangeCurrentView(vc: vc as? BaseViewController)
                UIView.transition(with: self.rootView, duration: 0.3, options: [.transitionCrossDissolve, .curveEaseInOut], animations: {
                    self.rootView.addSubview(vc.view)
                    vc.view.addFullScreenLayout()
                    self.addChild(vc)
                }, completion: nil)
                
            }
            while viewControllers.count > 1{
                viewControllers.removeLast()
            }
        }
    }
    
    func didChangeCurrentView(vc:BaseViewController?){
        if let vc = vc{
            if vc.useLogo{
                self.logoImageZeroHeightConstraint?.priority = UILayoutPriority(rawValue: 1)
            }
            else{
                self.logoImageZeroHeightConstraint?.priority = UILayoutPriority(rawValue: 999)
            }
        }
    }
    
    func showViewController(viewController:UIViewController?, animated:Bool = true){
        if let current = currentViewController{
            current.view.cleanupLayouts()
            current.view.removeFromSuperview()
            current.removeFromParent()
        }
        if let vc = viewController as? BaseViewController{
            
            didChangeCurrentView(vc: vc)
            if animated{
                UIView.transition(with: self.rootView, duration: 0.3, options: [.transitionCrossDissolve, .curveEaseInOut], animations: {
                    vc.rootViewController = self
                    self.rootView.addSubview(vc.view)
                    vc.view.addFullScreenLayout()
                    self.addChild(vc)
                    self.viewControllers.append(vc)
                }, completion: nil)

            }
            else{
                vc.rootViewController = self
                self.rootView.addSubview(vc.view)
                vc.view.addFullScreenLayout()
                self.addChild(vc)
                self.viewControllers.append(vc)
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
