//
//  TopDetailViewController.swift
//  SUMAIFUL
//
//  Created by occlusion on 6/2/19.
//  Copyright © 2019 Sunny Gem Co., Ltd. All rights reserved.
//

import UIKit

class TopDetailViewController: BaseViewController {

    var information:Information?
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var contentImageView: UIImageView!
    @IBOutlet weak var linkLabel: UILabel!
    @IBOutlet weak var thumbIconView: AspectRatioImageView!
    override func viewDidLoad() {
        super.viewDidLoad()

        titleLabel.text = information?.text ?? ""
        dateLabel.text = information?.dateText ?? ""
        contentLabel.text = information?.content ?? ""
        thumbIconView.isUserInteractionEnabled = true
        thumbIconView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapThumb(_:))))
        if let info = information{
            
            self.thumbIconView.loadURL(url: info.images.first?.url)
            if let link = info.link{
                contentImageView.isHidden = true
                
                linkLabel.attributedText = NSAttributedString(string: link.absoluteString, attributes: [NSAttributedString.Key.underlineStyle:NSUnderlineStyle.single.rawValue])
                linkLabel.isUserInteractionEnabled = true
                linkLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapImage(_:))))
            }
            else if let pdf = info.pdfs.first{
                linkLabel.isHidden = true
                if let url = pdf.url{
                    contentImageView.loadURL(url: url)
                    contentImageView.isUserInteractionEnabled = true
                    contentImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapImage(_:))))
                }
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let top = self.rootViewController as? TopViewController{
            top.changeSmallIcon()
        }
    }
    
    @objc func tapThumb(_ sender:Any){
        if let info = information{
            if let url = info.images.first?.url{
                let vc = AlbumImageViewController()
                vc.imageFiles.append(url)
                self.tabBarController?.present(controller: vc)
            }
        }
    }

    @objc func tapImage(_ sender:Any){
        if let info = information{
            if let link = info.link{
                openURL(url: link)
            }
            else if let pdf = info.pdfs.first{
                if let url = pdf.url{
                    openURL(url: url, name:pdf.name)
                }
            }

        }
    }
    
    @IBAction func back(_ sender: Any) {
        rootViewController?.goBack()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
