//
//  TopInformationTableViewCell.swift
//  SUMAIFUL
//
//  Created by occlusion on 8/20/18.
//  Copyright © 2018 Sunny Gem Co., Ltd. All rights reserved.
//

import UIKit

class TopInformationTableViewCell: UITableViewCell {

    @IBOutlet weak var thumbView: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var typeIconView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func bind(info:Information){
        infoLabel.text = info.text
        dateLabel.text = info.dateText
        thumbView.loadURL(url: info.images.first?.url)
        
        if info.link != nil{
            typeIconView.image = UIImage(named:"icon_link")
        }
        else{
            typeIconView.image = UIImage(named:"icon_pdf")
        }
    }

}
