//
//  TopRootViewController.swift
//  SUMAIFUL
//
//  Created by occlusion on 6/2/19.
//  Copyright © 2019 Sunny Gem Co., Ltd. All rights reserved.
//

import UIKit
import ESPullToRefresh

class TopRootViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {

    var currentInfoPage = 0
    var informations = [Information]()

    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.es.addPullToRefresh {[unowned self] in
            self.reloadData {
                self.tableView.es.stopPullToRefresh()
            }
        }

        reloadData()
    }
    func reloadData(_ callback:(()->Void)? = nil){
        self.tableView.willLoadData()
        Client.shared.fetchTopInformation { (info, err) in
            DispatchQueue.main.async {
                self.informations = info
                self.tableView.didEndLoadData()
                callback?()
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let top = self.rootViewController as? TopViewController{
            top.changeNormalIcon()
        }
    }

    func loadNextPageForInfo(){
        Client.shared.fetchTopInformation(page: currentInfoPage+1) { (info, err) in
            DispatchQueue.main.async {
                self.informations = info
                self.tableView.reloadSections(IndexSet(integer: 0), with: .automatic)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = UIColor.clear
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return informations.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MainCell", for: indexPath)
        let info = informations[indexPath.row]
        if let c = cell as? TopInformationTableViewCell{
            c.bind(info: info)
            c.thumbView.clear()
            c.thumbView.image = UIImage(named: "icon_information")
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 98
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "Detail") as? TopDetailViewController{
            vc.information = informations[indexPath.row]
            self.rootViewController?.showViewController(viewController: vc, animated:true)
        }

        tableView.deselectRow(at: indexPath, animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
