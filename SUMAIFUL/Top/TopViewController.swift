//
//  TopViewController.swift
//  SUMAIFUL
//
//  Created by occlusion on 8/19/18.
//  Copyright © 2018 Sunny Gem Co., Ltd. All rights reserved.
//

import UIKit


class TableViewPageAdapter{
    
    var currentPage = 0
    var isAllLoaded = false
    var isLoading = false
    
};

class TopViewController: TabBaseViewController {

    @IBOutlet weak var bigIconView: UIImageView!
    
    @IBOutlet weak var userTypeIconView: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var titleIconView: UIImageView!
    
    override func setup() {
        self.tabBarItem.selectedImage = UIImage(named: "tab_icon_home_on")?.withRenderingMode(.alwaysOriginal)
        self.tabBarItem.image = UIImage(named: "tab_icon_home_off")?.withRenderingMode(.alwaysOriginal)
    }
    
    func changeNormalIcon(){
        titleIconView.image = UIImage(named:"title_logo")
        bigIconView.image = UIImage(named: "icon_top")
    }
    func changeSmallIcon(){
        titleIconView.image = UIImage(named:"title_logo")
        bigIconView.image = nil
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userTypeIconView?.image = UIImage(named: "icon_user_type_\(Client.shared.myUser.type)")


        showViewController(viewController: self.storyboard?.instantiateViewController(withIdentifier: "Root"), animated:false)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
