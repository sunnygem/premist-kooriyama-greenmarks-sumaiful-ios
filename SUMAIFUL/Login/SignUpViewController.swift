//
//  SignUpViewController.swift
//  SUMAIFUL
//
//  Created by occlusion on 8/16/18.
//  Copyright © 2018 Sunny Gem Co., Ltd. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    var isFamilyUser = false
    var inputDataList = [InputData]()
    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var acknowledgeButton: UIButton!
    
    @IBOutlet weak var termOfUseButton: UIButton!
    @IBOutlet weak var privacyPolicyButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        inputDataList.append(InputData(title: NSLocalizedString("Room Number", comment: "")))
        inputDataList.append(InputData(title: NSLocalizedString("Sumaiful ID", comment: "")))
        inputDataList.append(InputData(title: NSLocalizedString("Password", comment: ""), secret:true))
        inputDataList.append(InputData(title: NSLocalizedString("Nickname", comment: "")))
        inputDataList.append(InputData(title: NSLocalizedString("Family Key", comment: "")))
        registerButton.clipsToBounds = true
        registerButton.layer.cornerRadius = registerButton.frame.height/2
        
        termOfUseButton.layer.borderWidth = 1
        termOfUseButton.layer.borderColor = UIColor(red: 0x7f/255.0, green: 0x7b/255.0, blue: 0x6f/255.0, alpha: 1.0).cgColor
        privacyPolicyButton.layer.borderWidth = 1
        privacyPolicyButton.layer.borderColor = UIColor(red: 0x77/255.0, green: 0x78/255.0, blue: 0x78/255.0, alpha: 1.0).cgColor
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !isFamilyUser{
            if inputDataList.last?.title == NSLocalizedString("Family Key", comment: ""){
                inputDataList.removeLast()
                tableView.reloadData()
            }
        }
    }

    @IBAction func goBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func register(_ sender: Any) {
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignedUpModal") as? SignedUpModalViewController{
            vc.didTapCallback = {
                vc.dismiss(animated: true, completion: {
                    self.performSegue(withIdentifier: "Main", sender: nil)
                })
            }
            self.definesPresentationContext = true
            vc.modalTransitionStyle = .crossDissolve
            vc.modalPresentationStyle = .overCurrentContext
            self.present(vc, animated: true, completion: nil)
            vc.titileLabel.text = NSLocalizedString("SignUpCompleteTitle", comment: "")
            vc.messageLabel.text = NSLocalizedString("SignUpCompleteMessage", comment: "")
        }
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = UIColor.clear
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return inputDataList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MainCell", for: indexPath)
        if let c = cell as? InputTableViewCell{
            let d = inputDataList[indexPath.row]
            c.titleLabel.text = d.title
            c.data = d
            c.inputField.text = d.input
            c.inputField.isSecureTextEntry = d.secret
        }
        return cell
    }
    
    @IBAction func didTapAcknowledge(_ sender: Any) {
        acknowledgeButton.isSelected = !acknowledgeButton.isSelected
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
