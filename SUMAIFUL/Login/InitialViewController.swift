//
//  InitialViewController.swift
//  SUMAIFUL
//
//  Created by occlusion on 8/16/18.
//  Copyright © 2018 Sunny Gem Co., Ltd. All rights reserved.
//

import UIKit

class InitialViewController: UIViewController {

    @IBOutlet weak var familyButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        familyButton.titleLabel?.textAlignment = .center
//        familyButton.setAttributedTitle(NSAttributedString(string:"ご家族用承認キーをお持ちの方は\nこちら"), for: .normal)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func didTapNewUser(_ sender: Any) {
        self.performSegue(withIdentifier: "NewUser", sender: nil)
    }

    @IBAction func didTapContractor(_ sender: Any) {
        self.performSegue(withIdentifier: "SignIn", sender: nil)
    }

    @IBAction func didTapGoToMain(_ sender: Any) {
        self.performSegue(withIdentifier: "Main", sender: nil)
    }
    
    @IBAction func didTapFamilyUser(_ sender: Any) {
        self.performSegue(withIdentifier: "NewFamilyUser", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let signup = segue.destination as? SignUpViewController{
            signup.isFamilyUser = true
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
