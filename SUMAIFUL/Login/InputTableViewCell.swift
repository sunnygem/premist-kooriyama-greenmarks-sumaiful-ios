//
//  InputTableViewCell.swift
//  SUMAIFUL
//
//  Created by occlusion on 8/20/18.
//  Copyright © 2018 Sunny Gem Co., Ltd. All rights reserved.
//

import UIKit

class InputTableViewCell: UITableViewCell {

    var data: InputData?
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var inputField: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        inputField.addTarget(self, action: #selector(didChangeInput(sender:)), for: .editingChanged)
    }
    
    @objc func didChangeInput(sender:Any){
        data?.input = inputField.text ?? ""
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
