//
//  SignedUpModalViewController.swift
//  SUMAIFUL
//
//  Created by occlusion on 1/26/19.
//  Copyright © 2019 Sunny Gem Co., Ltd. All rights reserved.
//

import UIKit

class SignedUpModalViewController: UIViewController {

    @IBOutlet weak var titileLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    
    
    var didTapCallback:(()->Void)?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTap(_:))))
    }
    
    
    @objc func didTap(_ sender:Any){
        didTapCallback?()
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
