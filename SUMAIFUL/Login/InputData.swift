//
//  InputData.swift
//  SUMAIFUL
//
//  Created by occlusion on 8/20/18.
//  Copyright © 2018 Sunny Gem Co., Ltd. All rights reserved.
//

import UIKit

class InputData: NSObject {
    var title = ""
    var input = ""
    var secret = true
    
    init(title:String, secret:Bool = false) {
        self.title = title
        self.secret = secret
    }
}
