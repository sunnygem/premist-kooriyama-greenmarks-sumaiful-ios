//
//  LoginViewController.swift
//  SUMAIFUL
//
//  Created by occlusion on 8/16/18.
//  Copyright © 2018 Sunny Gem Co., Ltd. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var loginIdField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        loginButton.clipsToBounds = true
        loginButton.layer.cornerRadius = loginButton.frame.height/2
        
        loginIdField.textColor = .white
        passwordField.textColor = .white
        loginIdField.attributedPlaceholder = NSAttributedString(string: loginIdField.placeholder ?? "", attributes: [.foregroundColor:UIColor(white: 1.0, alpha: 0.5)])
        passwordField.attributedPlaceholder = NSAttributedString(string: passwordField.placeholder ?? "", attributes: [.foregroundColor:UIColor(white: 1.0, alpha: 0.5)])
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func login(_ sender: Any) {
        guard let loginId = loginIdField.text, let password = passwordField.text else{return}
        let progress = self.showProgress(message: NSLocalizedString("Logging in...", comment: ""))
        Client.shared.login(userId:loginId, password: password) { (user, error) in
            DispatchQueue.main.async {
                progress.hide(animated: true)
                if let err = error{
                    self.show(error: err)
                }
                else{
                    self.performSegue(withIdentifier: "Main", sender: nil)
                }
            }
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardFrameChange(_:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func keyboardFrameChange(_ n:Notification) {
        guard self.view != nil else{return}
        let d = n.userInfo!
        let r = (d[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let diff = self.view.frame.height - r.origin.y
        
        UIView.animate(withDuration: 0.2) {
            self.bottomConstraint.constant = diff
            self.view.layoutIfNeeded()
            self.scrollView.scrollRectToVisible(CGRect(x: 0, y: self.scrollView.contentSize.height-1, width: 1, height: 1), animated: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
//                self.scrollView.scrollRectToVisible(CGRect(x: 0, y: self.scrollView.contentSize.height-1, width: 1, height: 1), animated: true)
            })
            
        }
    }

}
