//
//  NewUserViewController.swift
//  SUMAIFUL
//
//  Created by occlusion on 1/26/19.
//  Copyright © 2019 Sunny Gem Co., Ltd. All rights reserved.
//

import UIKit

class NewUserViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func didTapSignUp(_ sender: Any) {
        self.performSegue(withIdentifier: "SignUp", sender: nil)
    }
    
    @IBAction func didTapSignIn(_ sender: Any) {
        self.performSegue(withIdentifier: "SignIn", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let signup = segue.destination as? SignUpViewController{
            
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
