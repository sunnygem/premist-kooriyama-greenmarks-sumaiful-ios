//
//  DisclaimerViewController.swift
//  SUMAIFUL
//
//  Created by occlusion on 2/25/19.
//  Copyright © 2019 Sunny Gem Co., Ltd. All rights reserved.
//

import UIKit
import WebKit
class DisclaimerViewController: SideMenuSubViewController {

    let webView = WKWebView()
    var contents = ""
    
    var url:URL?
    var html:String?
    
    let titleLabel = UILabel()
//    let contentView = UITextView()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.addSubview(titleLabel)
        self.view.addSubview(webView)
        
        titleLabel.text = self.title
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.addHorizontalCenterLayout()
        titleLabel.font = UIFont.boldSystemFont(ofSize: 24)
        titleLabel.addRelatedLayout(attribute: .top, toItem: topBorder, attribute: .bottom, constant: 20)
        
        
        webView.addRelatedLayout(attribute: .top, toItem: titleLabel, attribute: .bottom, constant: 20)
        webView.addRelatedLayout(attribute: .bottom, toItem: closeButton, attribute: .top, constant: -20)
        webView.addFitToSuperViewSideLayout(20)
        

        if let url = url{
            webView.load(URLRequest(url: url))
        }
        if let html = html{
            webView.loadHTMLString(html, baseURL: nil)
        }
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
