//
//  FamilyKeyGenerationViewController.swift
//  SUMAIFUL
//
//  Created by occlusion on 2/25/19.
//  Copyright © 2019 Sunny Gem Co., Ltd. All rights reserved.
//

import UIKit

class FamilyKeyGenerationViewController: SideMenuSubViewController {

    
    let descriptionLabel = UILabel()
    let keyLabel = UILabel()
    let copyButton = UIButton()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.addSubview(descriptionLabel)
        self.view.addSubview(keyLabel)
        self.view.addSubview(copyButton)
        
        descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        keyLabel.translatesAutoresizingMaskIntoConstraints = false
        copyButton.translatesAutoresizingMaskIntoConstraints = false

        descriptionLabel.text = NSLocalizedString("Family Key Generator", comment: "")
        descriptionLabel.textAlignment = .center
        descriptionLabel.numberOfLines = 0
        descriptionLabel.lineBreakMode = .byWordWrapping
       
        descriptionLabel.addRelatedLayout(attribute: .top, toItem: topBorder, attribute: .bottom, constant: 20)
        descriptionLabel.addFitToSuperViewSideLayout(20)
        
        keyLabel.textAlignment = .center
        keyLabel.backgroundColor = UIColor.lightGray
        keyLabel.addRelatedLayout(attribute: .top, toItem: descriptionLabel, attribute: .bottom, constant: 20)
        keyLabel.addFitToSuperViewSideLayout(20)
        keyLabel.addSizeLayout(.height, size: 40, relation: .equal)
        keyLabel.text = "0000000"
        
        copyButton.setTitle(NSLocalizedString("Copy to Clipboard", comment: ""), for: .normal)
        copyButton.addRelatedLayout(attribute: .top, toItem: keyLabel, attribute: .bottom, constant: 20)
        copyButton.addHorizontalCenterLayout()
        copyButton.contentEdgeInsets = UIEdgeInsets(top: 4, left: 20, bottom: 4, right: 20)
        copyButton.backgroundColor = UIColor(red: 0.36, green: 0.31, blue: 0.24, alpha: 1.0)
        copyButton.clipsToBounds = true
        

        copyButton.addTarget(self, action: #selector(copyKey(_:)), for: .touchUpInside)
        
        // Do any additional setup after loading the view.
    }
    
    @objc func copyKey(_ sender:Any){
        if let text = keyLabel.text{
            UIPasteboard.general.string = text
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        copyButton.layer.cornerRadius = copyButton.frame.height/2
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
