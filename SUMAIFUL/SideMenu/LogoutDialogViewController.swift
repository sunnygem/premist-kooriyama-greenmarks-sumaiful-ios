//
//  LogoutDialogViewController.swift
//  SUMAIFUL
//
//  Created by occlusion on 4/20/19.
//  Copyright © 2019 Sunny Gem Co., Ltd. All rights reserved.
//

import UIKit

class LogoutDialogViewController: UIViewController {

    @IBOutlet weak var titileLabel: UILabel!
    @IBOutlet weak var cancelButton: RoundButton!
    @IBOutlet weak var logoutButton: RoundButton!
    @IBOutlet weak var logoIconView: UIImageView!
    var didFinishLogout = false
    var didLogout:(()->Void)?

    override func awakeFromNib() {
        super.awakeFromNib()
        modalTransitionStyle = .crossDissolve
        modalPresentationStyle = .overCurrentContext
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()


        titileLabel.text = NSLocalizedString("Would you like to logout?", comment: "")
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTap(_:))))
    }
    
    @IBAction func cancel(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func logout(_ sender: Any) {
        logoIconView.isHidden = false
        logoutButton.isHidden = true
        cancelButton.isHidden = true
        
        Client.shared.logout()
        didFinishLogout = true
        titileLabel.text = NSLocalizedString("Logout is completed.", comment: "")
    }
    
    
    @objc func didTap(_ sender:Any){
    
        dismiss(animated: true) {
            if self.didFinishLogout{
                self.didLogout?()
            }
        }
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
