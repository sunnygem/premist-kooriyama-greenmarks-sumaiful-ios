//
//  SideMenuSubViewController.swift
//  SUMAIFUL
//
//  Created by occlusion on 2/25/19.
//  Copyright © 2019 Sunny Gem Co., Ltd. All rights reserved.
//

import UIKit

class SideMenuSubViewController: UIViewController {
    
    let userNameLabel = UILabel()
    let topIconImageView = UIImageView()
    
    let topBorder = UIView()
    
    let closeButton = UIButton()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        
        self.view.addSubview(userNameLabel)
        self.view.addSubview(topIconImageView)
        self.view.addSubview(topBorder)
        self.view.addSubview(closeButton)
        
        userNameLabel.text = Client.shared.myUser.name
        userNameLabel.translatesAutoresizingMaskIntoConstraints = false
        userNameLabel.addFitToSuperViewLeftLayout(20)
        
        topIconImageView.translatesAutoresizingMaskIntoConstraints = false
        topIconImageView.addFitToSuperViewSafeTopLayout(0)
        topIconImageView.addFitToSuperViewRightLayout(-20)
        topIconImageView.addRelatedLayout(attribute: .centerY, toItem: userNameLabel, attribute: .centerY, constant: 0)
        topIconImageView.image = UIImage(named: "icon_main_top")
        
        topBorder.translatesAutoresizingMaskIntoConstraints = false
        topBorder.addSizeLayout(.height, size: 2, relation: .equal)
        topBorder.addFitToSuperViewLeftLayout(8)
        topBorder.addFitToSuperViewRightLayout(-8)
        topBorder.addRelatedLayout(attribute: .top, toItem: topIconImageView, attribute: .bottom, constant: 8)
        topBorder.backgroundColor = UIColor.lightGray
        

        closeButton.translatesAutoresizingMaskIntoConstraints = false
        closeButton.addFitToSuperViewSafeBottomLayout(-20)
        closeButton.addFitToSuperViewRightLayout(-20)
        closeButton.setImage(UIImage(named:"button_close_menu"), for: .normal)
        
        
        closeButton.addTarget(self, action: #selector(close(_:)), for: .touchUpInside)

        // Do any additional setup after loading the view.
    }
    
    @objc func close(_ sender:Any){
        self.navigationController?.popViewController(animated: false)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
