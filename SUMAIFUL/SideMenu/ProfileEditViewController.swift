//
//  ProfileEditViewController.swift
//  SUMAIFUL
//
//  Created by occlusion on 2/25/19.
//  Copyright © 2019 Sunny Gem Co., Ltd. All rights reserved.
//

import UIKit

class ProfileEditViewController: SideMenuSubViewController {

    let profileImageButton = UIButton()
    let nicknameDot = UIImageView()
    let nicknameLabel = UILabel()
    let nicknameInput = UITextField()
    
    let changeButton = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.addSubview(profileImageButton)
        self.view.addSubview(nicknameDot)
        self.view.addSubview(nicknameLabel)
        self.view.addSubview(nicknameInput)
        self.view.addSubview(changeButton)

        profileImageButton.translatesAutoresizingMaskIntoConstraints = false
        nicknameDot.translatesAutoresizingMaskIntoConstraints = false
        nicknameInput.translatesAutoresizingMaskIntoConstraints = false
        nicknameLabel.translatesAutoresizingMaskIntoConstraints = false
        changeButton.translatesAutoresizingMaskIntoConstraints = false
        

        
        profileImageButton.addHorizontalCenterLayout()
        profileImageButton.setImage(UIImage(named: "icon_profile_blank"), for: .normal)
        profileImageButton.addRelatedLayout(attribute: .top, toItem: topBorder, attribute: .bottom, constant: 20)
        profileImageButton.addSizeLayout(.width, size: 128, relation: .equal)
        profileImageButton.addSizeLayout(.height, size: 128, relation: .equal)
        profileImageButton.imageView?.contentMode = .scaleAspectFit
        
        
        nicknameDot.image = UIImage(named:"icon_dot")
        nicknameDot.addFitToSuperViewLeftLayout(20)

        
        nicknameLabel.text = NSLocalizedString("Nickname", comment: "")
        nicknameLabel.addRelatedLayout(attribute: .centerY, toItem: nicknameDot, attribute: .centerY, constant: 0)
        nicknameLabel.addRelatedLayout(attribute: .top, toItem: profileImageButton, attribute: .bottom, constant: 20)
        nicknameLabel.addRelatedLayout(attribute: .left, toItem: nicknameDot, attribute: .right, constant: 4)
        
        nicknameInput.addRelatedLayout(attribute: .left, toItem: nicknameDot, attribute: .left, constant: 0)
        nicknameInput.addRelatedLayout(attribute: .top, toItem: nicknameLabel, attribute: .bottom, constant: 2)
        nicknameInput.addFitToSuperViewSideLayout(20)
        nicknameInput.addSizeLayout(.height, size: 40, relation: .equal)
        nicknameInput.borderStyle = .bezel
        
        changeButton.backgroundColor = UIColor(red: 0.36, green: 0.31, blue: 0.24, alpha: 1.0)
        changeButton.addRelatedLayout(attribute: .top, toItem: nicknameInput, attribute: .bottom, constant: 20)
        changeButton.setTitle(NSLocalizedString("Change", comment: ""), for: .normal)
        changeButton.addFitToSuperViewRightLayout(-20)
        changeButton.contentEdgeInsets = UIEdgeInsets(top: 8, left: 12, bottom: 8, right: 12)
        changeButton.clipsToBounds = true
        changeButton.layoutSubviews()
        changeButton.layer.cornerRadius = changeButton.frame.height/2

        
        changeButton.addTarget(self, action: #selector(change(_:)), for: .touchUpInside)
        // Do any additional setup after loading the view.
    }
    
    
    @objc func change(_ sender:Any){
        self.navigationController?.popViewController(animated: false)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        changeButton.layer.cornerRadius = changeButton.frame.height/2
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
