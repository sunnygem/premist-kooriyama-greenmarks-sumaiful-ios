//
//  SideMenuViewController.swift
//  SUMAIFUL
//
//  Created by occlusion on 8/19/18.
//  Copyright © 2018 Sunny Gem Co., Ltd. All rights reserved.
//

import UIKit

class SideMenuViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    class Item: NSObject {
        var name = ""
        var callback:(()->Void)
        init(name:String, callback:@escaping ()->Void) {
            self.name = name
            self.callback = callback
        }
    }
    
    @IBOutlet weak var userTypeImageView: UIImageView!
    var items = [Item]()
    var closeCallback:(()->Void)?

    @IBOutlet weak var closeButton: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        userTypeImageView.image = UIImage(named: "icon_user_type_\(Client.shared.myUser.type)")

        closeButton.isUserInteractionEnabled = true
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(close(sender:)))
        closeButton.addGestureRecognizer(tap)
        
        items.append(Item(name: NSLocalizedString("Term of Service", comment: ""), callback: {
            let tos = DisclaimerViewController()
            tos.title = NSLocalizedString("Term of Service", comment: "")
            tos.contents = "TERM TEST"
            if let html = Client.shared.termOfUseHtml, !html.isEmpty{
                tos.html = html
            }
            else if let url = Client.shared.termOfUseURL{
                tos.url = url
            }
            else{
                return
            }

            self.navigationController?.pushViewController(tos, animated: false)

        }))
        /*
        items.append(Item(name: NSLocalizedString("License", comment: ""), callback: {
            let license = DisclaimerViewController()
            license.title = NSLocalizedString("License", comment: "")
            self.navigationController?.pushViewController(license, animated: false)

        }))
 */
        items.append(Item(name: NSLocalizedString("Logout", comment: ""), callback: {
            DispatchQueue.main.async {
                
                if let vc = self.storyboard?.instantiateViewController(withIdentifier: "LogoutDialog") as? LogoutDialogViewController{
                    self.definesPresentationContext = true
                    vc.didLogout = {
                        Client.shared.logout()
                        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "Login"){
                            self.tabBarController?.navigationController?.setViewControllers([vc], animated: true)
                        }
                    }
                    self.tabBarController?.navigationController?.present(vc, animated: true, completion: nil)
                }
                
            }
            
        }))

    }
    
    override func awakeFromNib() {
    }
    
    @objc func close(sender:Any){
        closeCallback?()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MainCell", for: indexPath)
        if let c = cell as? SideMenuTableViewCell{
            let item = items[indexPath.row]
            c.titleLabel.text = item.name
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = items[indexPath.row]
        item.callback()
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 64
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
