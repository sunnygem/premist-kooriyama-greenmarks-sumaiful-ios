//
//  SideMenuTableViewCell.swift
//  SUMAIFUL
//
//  Created by occlusion on 8/20/18.
//  Copyright © 2018 Sunny Gem Co., Ltd. All rights reserved.
//

import UIKit

class SideMenuTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
