//
//  WebViewController.swift
//  SUMAIFUL
//
//  Created by occlusion on 1/30/19.
//  Copyright © 2019 Sunny Gem Co., Ltd. All rights reserved.
//

import UIKit
import WebKit
import SafariServices
class WebViewController: UIViewController, WKNavigationDelegate, WKUIDelegate {
    var webView: WKWebView!
    
    var url:URL?
    let indicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.gray)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let config = WKWebViewConfiguration()
        webView = WKWebView(frame: self.view.bounds, configuration: config)
        self.view.addSubview(webView)
        webView.addObserver(self, forKeyPath: "loading", options: .new, context: nil)
        webView.uiDelegate = self
        webView.navigationDelegate = self
        webView.translatesAutoresizingMaskIntoConstraints = false
        webView.addFitToSuperViewSafeTopLayout()
        webView.addFitToSuperViewSideLayout()
        webView.addFitToSuperViewBottomLayout()
        
        if let url = url{
            webView.load(URLRequest(url: url))
        }
        self.view.addSubview(indicator)
        indicator.hidesWhenStopped = true
        
        indicator.addCenterLayout()
        
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "loading"{
            if webView.isLoading{
                indicator.startAnimating()
            }
            else{
                indicator.stopAnimating()
            }
        }
    }
    
    
    func loadURL(url:URL){
        if webView != nil{
            webView.load(URLRequest(url: url))
        }
        else{
            self.url = url
        }
    }
    
    func webView(_ webView: WKWebView, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        completionHandler(.useCredential, URLCredential(user: Client.shared.BasicUser, password: Client.shared.BasicPassword, persistence: .permanent))
    }

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
}


extension UIViewController{
    func openURL(url:URL, name:String = ""){
        if url.absoluteString.starts(with: Client.shared.endpoint){
            let webView = WebViewController()
            webView.title = name
            webView.loadURL(url: url)
            if let navi = self.navigationController{
                navi.pushViewController(webView, animated: true)
            }
            else{
                self.present(webView, animated: true, completion: nil)
            }
        }
        else{
            let webView = SFSafariViewController(url: url)
            webView.title = name
            self.present(webView, animated: true, completion: nil)
        }
    }
}
