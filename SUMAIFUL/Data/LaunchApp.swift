//
//  LaunchApp.swift
//  SUMAIFUL
//
//  Created by occlusion on 8/20/18.
//  Copyright © 2018 Sunny Gem Co., Ltd. All rights reserved.
//

import UIKit

class LaunchApp: NSObject {
    var icon:UIImage?
    var name = ""
    var appId = ""
    var scheme: URL?
    var isFavorite = false{
        didSet{
            UserDefaults.standard.set(isFavorite, forKey: appId)
        }
    }
}
