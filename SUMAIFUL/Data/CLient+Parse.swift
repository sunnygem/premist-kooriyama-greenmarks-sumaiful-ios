//
//  CLient+Parse.swift
//  SUMAIFUL
//
//  Created by occlusion on 4/19/19.
//  Copyright © 2019 Sunny Gem Co., Ltd. All rights reserved.
//

import UIKit

extension Client{
    
    func parseInformation(_ data:[String:Any]?)->Information?{
        guard let data = data else{return nil}
        let info = Information()
        info.text = data["title"] as? String ?? ""
        
        if let images = data["images"] as? [[String:Any]]{
            for image in images{
                let name = image["name"] as? String ?? ""
                let url = parseUrl(path: image["file_path"])
                info.addImage(name: name, url: url)
            }
        }
        if let pdfs = data["pdfs"] as? [[String:Any]]{
            for pdf in pdfs{
                let name = pdf["name"] as? String ?? ""
                let url = parseUrl(path: pdf["file_path"])
                info.addPdf(name: name, url: url)
            }
        }
        
        info.link = parseUrl(path: data["link_url"])
        info.content = data["content"] as? String ?? ""
        info.date = self.parseDate(data["release_date"])
        info.dateText = self.presentationDateFormatter.string(from: info.date!)
        
        return info
    }
    
    
    func parseAppList(dataList:[[String:Any]])->[LaunchApp]{
        var list = [LaunchApp]()
        
        for data in dataList{
            let app = LaunchApp()
            let scheme = data["scheme_ios"] as? String ?? ""
            app.name = data["name"] as? String ?? ""
            app.appId = data["app_id_ios"] as? String ?? ""
            if app.appId.hasPrefix("id"){
                app.appId = String(app.appId.suffix(app.appId.count-2))
            }
            app.scheme = URL(string:scheme)
            app.isFavorite = UserDefaults.standard.bool(forKey: app.appId)
            /*
            let escaped = scheme.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
            app.url = URL(string: escaped)
            if let files = data["files"] as? [[String:Any]]{
                for file in files{
                    app.iconUrl = self.parseUrl(path: file["file_path"])
                    break
                }
            }
 */
            list.append(app)
        }
        return list
    }
    

    func parseAccessory(_ data:[String:Any]?)->Accessory?{
        guard let data = data else{return nil}
        
        let accessory = Accessory()
        accessory.name = data["label"] as? String ?? ""
        accessory.memo = data["memo"] as? String ?? ""
        accessory.information = data["label_en"] as? String ?? ""
        accessory.appList = self.parseAppList(dataList: data["menu_applications"] as? [[String:Any]] ?? [])
        accessory.icon = self.parseUrl(path: data["service_menu_icon_path"])

        return accessory
    }
    
    func parseManual(_ data:[String:Any]?)->ConciergeManuals.Manual?{
        guard let data = data else{return nil}
        let manualObj = ConciergeManuals.Manual()
        manualObj.date = self.parseDate(data["created"])
        if let name = data["equipment_name"] as? String{
            manualObj.name = name
        }
        for path in data["files"] as? [[String:Any]] ?? []{
            if let url = self.parseUrl(path: path["file_path"]){
                manualObj.files.append(url)
            }
        }
        return manualObj
    }
    
    func parseManualCategory(_ data:[String:Any]?)->ManualCategory?{
        guard let data = data else{return nil}
        
        let category = ManualCategory()
        category.title = data["name"] as? String ?? ""
        category.id = data["id"] as? Int ?? 0
        category.icon = self.parseUrl(path: data["icon_path"])
        return category
    }
    func parseConcierge(_ data:[String:Any]?)->Concierge?{
        guard let data = data else{return nil}
        
        let concierge = Concierge()
        concierge.name = data["label"] as? String ?? ""
        concierge.information = data["label_en"] as? String ?? ""
        concierge.icon = self.parseUrl(path: data["service_menu_icon_path"])
        concierge.type = ConciergeType(rawValue: data["service_menu_id"] as? Int ?? 0) ?? .unknown
        
        switch concierge.type {
        case .contactPhone:
            let contactData = ConciergeContacts()
            if let data = data["menu_contact_tel"] as? [String:Any]{
                contactData.tel = data["tel"] as? String ?? ""
                contactData.title = data["property_url_label"] as? String ?? ""
                contactData.url = URL(string:data["property_url"] as? String ?? "")
                contactData.reserveTitle = data["attend_reserve_url_label"] as? String ?? ""
                contactData.reserveUrl = URL(string:data["attend_reserve_url"] as? String ?? "")
                contactData.comment = data["comment"] as? String ?? ""
            }
            concierge.data = contactData
            
        case .app:
            let applist = self.parseAppList(dataList: data["menu_applications"] as? [[String:Any]] ?? [])
            let appData = ConciergeApplications()
            appData.apps = applist
            appData.memo = data["memo"] as? String ?? ""
            concierge.data = appData
            
        case .manual:
            let manualData = ConciergeManuals()
            for manual in data["menu_manuals"] as? [[String:Any]] ?? []{
                let manualObj = ConciergeManuals.Manual()
                manualObj.date = self.parseDate(data["created"])
                if let name = manual["equipment_name"] as? String{
                    manualObj.name = name
                }
                for path in manual["files"] as? [[String:Any]] ?? []{
                    if let url = self.parseUrl(path: path["file_path"]){
                        manualObj.files.append(url)
                    }
                }
                manualData.manuals.append(manualObj)
            }
            concierge.data = manualData

        case .survey:
            let surveyData = ConciergeSurvey()
            
            for survey in data["menu_enquetes"] as? [[String:Any]] ?? []{
                let obj = ConciergeSurvey.Survey()
                obj.id = survey["id"] as? Int ?? 0
                obj.title = survey["title"] as? String ?? ""
                surveyData.surveys.append(obj)
            }
            
            concierge.data = surveyData
        case .link:
            let linkData = ConciergeLinks()
            
            for link in data["menu_linktypes"] as? [[String:Any]] ?? []{
                if let url = self.parseUrl(path: link["link_url"]){
                    linkData.links.append(url)
                }
            }
            concierge.data = linkData

        default:
            concierge.data = nil
        }
        
        
        return concierge
        
    }
    
    func parseMoreSumaiful(_ data:[String:Any]?)->MoreSumaiful?{
        guard let data = data else{return nil}
        
        let info = MoreSumaiful()
        info.text = data["title"] as? String ?? ""
        
        if let images = data["files"] as? [[String:Any]]{
            for image in images{
                let name = image["name"] as? String ?? ""
                let url = self.parseUrl(path: image["file_path"])
                info.addImage(name: name, url: url)
            }
        }
        
        info.contents = data["html_content"] as? String ?? ""
        
        let dateStr = data["release_date"] as? String ?? ""
        info.date = self.parseDate(data["release_date"])
        info.dateText = self.presentationDateFormatter.string(from: info.date!)
        
        return info
    }
}
