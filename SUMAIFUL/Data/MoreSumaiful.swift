//
//  MoreSumaiful.swift
//  SUMAIFUL
//
//  Created by occlusion on 3/1/19.
//  Copyright © 2019 Sunny Gem Co., Ltd. All rights reserved.
//

import UIKit

class MoreSumaiful: NSObject {
    class Image{
        var name = ""
        var url:URL?
        init(name:String, url:URL?){
            self.name = name
            self.url = url
        }
    }
    
    var icon: UIImage?
    var date: Date?
    var dateText = ""
    var text = ""
    var contents = ""
    
    var images = [Image]()
    
    
    func addImage(name:String, url:URL?){
        images.append(Image(name:name, url:url))
    }
}
