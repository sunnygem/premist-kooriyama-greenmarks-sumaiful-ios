//
//  Information.swift
//  SUMAIFUL
//
//  Created by occlusion on 8/20/18.
//  Copyright © 2018 Sunny Gem Co., Ltd. All rights reserved.
//

import UIKit

class Information: NSObject {
    class Image{
        var name = ""
        var url:URL?
        init(name:String, url:URL?){
            self.name = name
            self.url = url
        }
    }
    
    class Pdf{
        var name = ""
        var url:URL?
        init(name:String, url:URL?){
            self.name = name
            self.url = url
        }
    }

    var icon: UIImage?
    var date: Date?
    var dateText = ""
    var text = ""
    var content = ""
    var link:URL?
    
    var images = [Image]()
    var pdfs = [Pdf]()
    
    
    func addImage(name:String, url:URL?){
        images.append(Image(name:name, url:url))
    }
    func addPdf(name:String, url:URL?){
        pdfs.append(Pdf(name:name, url:url))
    }
}
