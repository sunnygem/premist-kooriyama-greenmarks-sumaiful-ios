//
//  ClientBase.swift
//  willing
//
//  Created by occlusion on 4/19/19.
//  Copyright © 2019 Sunny Gem Co., Ltd. All rights reserved.
//

import UIKit
import Alamofire

extension Dictionary {
    public func union(other: Dictionary) -> Dictionary {
        var tmp = self
        other.forEach { tmp[$0.0] = $0.1 }
        return tmp
    }
}

class ClientBase: NSObject {
    var endpoint = ""
    var EncryptSecret = ""
    var EncryptSharedPassword = ""
    
    var BasicUser = ""
    var BasicPassword = ""
    var UniqueParameter = ""
    
    var apiUrl:String{
        return endpoint + "api/v1/0/"
    }
    
    var deviceToken:String?
    var isHeldLogin = false{
        didSet{
            if oldValue != isHeldLogin{
                if !isHeldLogin{
                    //                    if let data = lastLoginData, let cb = self.lastLoginCallback{
                    //                        self.login(data, cb)
                    //                    }
                }
            }
        }
    }
    
    var basicHeaders = [String:String]()
    var accessToken:String? = nil
    var refreshToken:String? = nil
    
    
    let dateFormatter = DateFormatter()
    let myUser = User()
    
    
    override init() {
        super.init()
    }
    
    
    func logout(){
        accessToken = nil
        refreshToken = nil
        save()
        myUser.copy(user: User())
    }
    
    func setup(){
        let credentialData = "\(BasicUser):\(BasicPassword)".data(using: String.Encoding.utf8)!
        let base64Credentials = credentialData.base64EncodedString(options: [])
        basicHeaders = ["Authorization": "Basic \(base64Credentials)"]
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ssXXXXX"
        
        //        presentationDateFormatter.dateFormat = "yyyy.MM.dd"
        
    }
    
    
    func relogin(_ callback:@escaping (Bool, Error?)->Void){
        refreshAccessToken { (err) in
            if let err = err{
                callback(false, err)
            }
            else{
                self.fetchUser(userId: nil, callback: { (user, error) in
                    if let user = user{
                        self.myUser.copy(user: user)
                        self.didLogin()
                        callback(true, err)
                    }
                    else{
                        callback(false, err)
                    }
                })
            }
        }
    }
    
    func didLogin(){
    }
    
    func save(){
        UserDefaults.standard.set(accessToken, forKey: "Access")
        UserDefaults.standard.set(refreshToken, forKey: "Refresh")
        UserDefaults.standard.synchronize()
    }
    
    func load(){
        self.accessToken = UserDefaults.standard.object(forKey: "Access") as? String
        self.refreshToken = UserDefaults.standard.object(forKey: "Refresh") as? String
    }
    
    
    
    func parameters(_ data:[String:String] = [:])->[String:String]{
        return ["access_token":accessToken ?? "", "unique_parameter":UniqueParameter].union(other: data)
    }
    
    func fetchAccessToken(_ callback:@escaping (Error?)->Void){
        Alamofire.request(apiUrl + "get_token", method: .post, parameters:
            ["unique_parameter":UniqueParameter], headers:basicHeaders).responseJSON { (response) in
                self.parseResponse(response: response, callback: { (result, error) in
                    if let data = result.first{
                        self.accessToken = data["access_token"] as? String ?? ""
                        self.refreshToken = data["refresh_token"] as? String ?? ""
                        self.save()
                    }
                    callback(error)
                }, retryCallback: {})
        }
    }
    
    func refreshAccessToken(_ callback:@escaping (Error?)->Void){
        if self.refreshToken == nil{
            self.fetchAccessToken { (err) in
                if let err = err{
                    callback(err)
                }
                else{
                    self.refreshAccessToken(callback)
                }
            }
            return
        }
        
        Alamofire.request(apiUrl + "get_refresh_token", method: .post, parameters:
            ["refresh_token":self.refreshToken ?? "", "unique_parameter":UniqueParameter], headers:basicHeaders).responseJSON { (response) in
                self.parseResponse(response: response, callback: { (result, error) in
                    if let data = result.first{
                        self.accessToken = data["access_token"] as? String ?? ""
                        self.refreshToken = data["refresh_token"] as? String ?? ""
                        self.save()
                    }
                    callback(error)
                }, retryCallback: {})
        }
    }
    
    func parseResponse(response:DataResponse<Any>, callback:@escaping ([[String:Any]] , Error?)->Void, retryCallback:@escaping ()->Void){
        #if DEBUG
        /*
         if let data = response.data{
         let txt = String(data: data, encoding: .utf8)
         print(txt)
         }
         //         // */
        #endif
        if let result = response.result.value as? [String:Any]{
            if let code = Int(result["code"] as? String ?? ""){
                if code == 200{} //No Error
                else if code == 001{
                    self.accessToken = nil
                    retryCallback()
                    return
                }
                else if code == 002{
                    self.fetchAccessToken({ (err) in
                        if let err = err{
                            callback([], err)
                            return
                        }
                        retryCallback()
                    })
                    return
                }
                else{
                    callback([], NSError(domain: "jp.sunnygem.api", code: code, userInfo: [NSLocalizedDescriptionKey:self.errorReason(code)]))
                    print("Error code:\(code) \(self.errorReason(code))")
                    return
                }
            }
            if let r = result["data"] as? [[String:Any]]{
                callback(r, response.error)
                return
            }
            if let r = result["data"] as? [String:Any]{
                callback([r], response.error)
                return
            }
        }
        callback([], response.error)
    }
    
    func errorReason(_ code:Int)->String{
        switch code {
        case 1: return NSLocalizedString("ErrorRequestRefreshToken", comment: "")
        case 2: return NSLocalizedString("ErrorRequestLogin", comment: "")
        case 3: return NSLocalizedString("ErrorInvalidDataParameter", comment: "")
        case 4: return NSLocalizedString("ErrorInvalidUniqueParameter", comment: "")
        case 5: return NSLocalizedString("ErrorInvalidAccessToken", comment: "")
        case 6: return NSLocalizedString("ErrorMethodDoesNotExist", comment: "")
        case 7: return NSLocalizedString("ErrorPostNotAvailable", comment: "")
        case 8: return NSLocalizedString("ErrorInvalidAction", comment: "")
        case 9: return NSLocalizedString("ErrorFailedRegistData", comment: "")
        case 10: return NSLocalizedString("ErrorMissingLoginData", comment: "")
        case 11: return NSLocalizedString("ErrorInavalidEncryptionParameter", comment: "")
        case 12: return NSLocalizedString("ErrorIncorrectLoginData", comment: "")
        default: return NSLocalizedString("ErrorUnknown", comment: "")
        }
    }
    
    func upload(url:String, dataList:[String:Data], parameters:[String:String], callback:@escaping ([[String:Any]] , Error?)->Void){
        if self.accessToken == nil{
            self.refreshAccessToken { (err) in
                if let err = err{
                    callback([], err)
                }
                else{
                    self.upload(url: url, dataList:dataList, parameters: parameters, callback: callback)
                }
            }
            return
        }
        
        
        Alamofire.upload(multipartFormData: { (multipart) in
            for data in dataList{
                multipart.append(data.value, withName: data.key, fileName: "upload\(arc4random()).jpg", mimeType: "image/jpeg")
            }
            for param in self.parameters(parameters){
                multipart.append(param.value.data(using: .utf8)!, withName: param.key, mimeType:"plain/text")
            }
        }, to: url,
           method:.post,
           headers:basicHeaders
        ) { (result) in
            switch result{
            case .success(request: let upload, streamingFromDisk: _, streamFileURL: _):
                upload.responseJSON(completionHandler: { (response) in
                    self.parseResponse(response: response, callback: callback, retryCallback: {
                        self.upload(url: url, dataList: dataList, parameters: parameters, callback: callback)
                    })
                })
            case .failure(let err):
                callback([[:]], err)
            }
        }
    }
    
    
    func call(url:String, parameters:[String:String], callback:@escaping ([[String:Any]] , Error?)->Void){
        if self.accessToken == nil{
            self.refreshAccessToken { (err) in
                if let err = err{
                    callback([], err)
                }
                else{
                    self.call(url: url, parameters: parameters, callback: callback)
                }
            }
            return
        }
        Alamofire.request(url, method: .post, parameters:
            self.parameters(parameters), headers:basicHeaders).responseJSON { (response) in
                self.parseResponse(response: response, callback: callback, retryCallback: {
                    self.call(url: url, parameters: parameters, callback: callback)
                })
        }
    }
    
    
    func fetchEncryptData(callback:@escaping (Int, String, String, Error?)->Void){
        self.call(url: apiUrl + "get_encryption_parameters", parameters: [:]) { (data, error) in
            if let d = data.first{
                let id = d["id"] as? Int ?? -1
                let iv = d["iv"] as? String ?? ""
                let salt = d["salt"] as? String ?? ""
                
                callback(id, iv, salt, error)
            }
            else{
                callback(-1, "", "", error)
            }
        }
    }
    
    
    
    func parseLoginReply(data:[String:Any]?, callback:@escaping (User?, Error?)->Void)->Bool{
        /*
         if let user = self.parseUser(data: data.first){
         self.myUser.copy(user: user)
         self.didLogin()
         self.save()
         callback(self.myUser, error)
         return true
         }
         return false
         */
        return false
    }
    
    func login(userId:String, password:String, callback:@escaping (User?, Error?)->Void){
        
        fetchEncryptData { (id, iv, salt, error) in
            if let err = error{
                callback(nil, err)
                return
            }
            
            let encrypted = EncryptionHelper.encrypt(self.EncryptSecret+password, password: self.EncryptSharedPassword, iv: iv, salt: salt, info: "")
            //            let dec = EncryptionHelper.decrypt(encrypted, password: sharedPass, iv: iv, salt: salt, info: "")
            
            self.call(url: self.apiUrl + "login", parameters: ["login_id":userId, "password":encrypted, "encryption_parameter_id":"\(id)", "device_token":self.deviceToken ?? "", "os":"ios"]) { (data, error) in
                if !self.parseLoginReply(data: data.first, callback: callback){
                    callback(nil, error)
                }
            }
        }
    }
    
    
    func fetchUser(userId:Int?, callback:@escaping (User?, Error?)->Void){
        var params = [String:String]()
        if let userId = userId{
            params["user_id"] = "\(userId)"
        }
        self.call(url: apiUrl + "get_front_user", parameters: params) { (data, error) in
            if let user = data.first{
                callback(self.parseUser(data: user), error)
            }
            else{
                callback(nil, error)
            }
        }
    }
    
    
    func parseDate(_ string:Any?)->Date{
        guard let string = string as? String else{return Date()}
        
        return self.dateFormatter.date(from: string.replacingOccurrences(of: "T", with: " ")) ?? Date()
    }
    
    func parseUrl(path:Any?)->URL?{
        guard let path = path as? String else{return nil}
        if path.isEmpty{return nil}
        if path.hasPrefix("http"){
            return URL(string:path)
        }
        return URL(string: endpoint + path)
    }
    
    
    func parseUser(data:[String:Any]?)->User?{
        return nil
    }
}
