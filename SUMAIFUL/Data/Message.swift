//
//  Message.swift
//  SUMAIFUL
//
//  Created by occlusion on 2/5/19.
//  Copyright © 2019 Sunny Gem Co., Ltd. All rights reserved.
//

import UIKit

class Message: NSObject {
    
    var id = 0
    var user: User?
    var contents = ""
    var date = Date()
    var dateStr = ""
    var isIncoming = false

}
