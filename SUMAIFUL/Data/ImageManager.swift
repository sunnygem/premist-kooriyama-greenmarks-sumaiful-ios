//
//  ImageManager.swift
//  SUMAIFUL
//
//  Created by occlusion on 1/30/19.
//  Copyright © 2019 Sunny Gem Co., Ltd. All rights reserved.
//


import UIKit
import Alamofire
class ImageManager: NSObject {
    
    static let shared = ImageManager()
    var maximum = 100
    var hashList = [String]()
    var paths = [String:String]()
    var directory = ""
    
    override init() {
        super.init()
        if let dir = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).last{
            directory = (dir as NSString).appendingPathComponent("Images")
            _ = try? FileManager.default.createDirectory(atPath: directory, withIntermediateDirectories: true, attributes: nil)
        }
        self.load()
    }
    
    func save(){
        objc_sync_enter(self)
        let path = directory.appendingFormat("/data.dat")
        _ = try? NSKeyedArchiver.archivedData(withRootObject: ["HashList":hashList, "Paths":paths]).write(to: URL(fileURLWithPath: path), options: .atomicWrite)
        objc_sync_exit(self)
    }
    
    func load(){
        objc_sync_enter(self)
        let path = directory.appendingFormat("/data.dat")
        if case let data as [String:Any] = NSKeyedUnarchiver.unarchiveObject(withFile: path){
            if case let hashList as [String] = data["HashList"]{
                self.hashList = hashList
            }
            if case let paths as [String:String] = data["Paths"]{
                self.paths = paths
            }
        }
        objc_sync_exit(self)
    }
    
    
    
    func convertPDFPageToImage(pdf:Data, page:Int = 1) -> UIImage? {
        
        let pdfData = pdf as CFData
        guard let provider:CGDataProvider = CGDataProvider(data: pdfData) else{return nil}
        guard let pdfDoc:CGPDFDocument = CGPDFDocument(provider) else{return nil}
        //        let pageCount = pdfDoc.numberOfPages
        guard let pdfPage:CGPDFPage = pdfDoc.page(at: page) else{return nil}
        var pageRect:CGRect = pdfPage.getBoxRect(.mediaBox)
        pageRect.size = CGSize(width:pageRect.size.width, height:pageRect.size.height)
        
        
        UIGraphicsBeginImageContextWithOptions(pageRect.size, true, UIScreen.main.scale)
        let context:CGContext = UIGraphicsGetCurrentContext()!
        context.setFillColor(UIColor.white.cgColor)
        context.fill(pageRect)
        context.saveGState()
        context.translateBy(x: 0.0, y: pageRect.size.height)
        context.scaleBy(x: 1.0, y: -1.0)
        context.concatenate(pdfPage.getDrawingTransform(.mediaBox, rect: pageRect, rotate: 0, preserveAspectRatio: true))
        context.drawPDFPage(pdfPage)
        context.restoreGState()
        let pdfImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        let data = pdfImage.jpegData(compressionQuality: 0.8)
        return UIImage(data: data!)
        //        return pdfImage
    }
    
    func imageForURL(url:URL, progressCallback:((Double)->Void)? = nil) -> UIImage? {
        var key = url.path
        if !url.isFileURL{
            key = url.absoluteString
            if let image = self.image(key: url.absoluteString){
                return image
            }
        }
        
        var data:NSData? = nil
        let semaphore = DispatchSemaphore(value: 0)
        DispatchQueue.global(qos: .default).async {
            Client.shared.fetchDataRequest(url: url).downloadProgress(closure: { (progress) in
                progressCallback?(progress.fractionCompleted)
            }) .responseData(completionHandler: { (result) in
                data = result.data as NSData?
                semaphore.signal()
            })
        }
        semaphore.wait()
        //        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER)
        if let data = data{
            if url.pathExtension == "pdf"{
                if let image = self.convertPDFPageToImage(pdf: data as Data){
                    self.addImage(image: image, key: key)
                    return image
                }
            }
            else if let image = UIImage(data: data as Data){
                self.addImage(image: image, key: key)
                return image
            }
        }
        return nil
    }
    
    func addImage(image:UIImage, key:String){
        var name = key.replacingOccurrences(of: ":", with: "")
        name = name.replacingOccurrences(of: "/", with: "")
        name = name.replacingOccurrences(of: "-", with: "")
        if (key as NSString).pathExtension.count != 0 && (key as NSString).pathExtension.count != 3{
            name = (name as NSString).deletingPathExtension
        }
        name = "/\(name).png"
        let path = directory.appending(name)
        _ = try? image.pngData()?.write(to: URL(fileURLWithPath: path), options: .atomicWrite)
        
        objc_sync_enter(self)
        paths[key] = name
        if let idx = hashList.index(of: key){
            hashList.remove(at: idx)
        }
        hashList.append(key)
        objc_sync_exit(self)
        self.save()
    }
    
    func image(key:String)->UIImage?{
        
        objc_sync_enter(self)
        if let path = paths[key]{
            
            if let data = NSData(contentsOfFile:"\(directory)\(path)"){
                objc_sync_exit(self)
                return UIImage(data: data as Data)
            }
        }
        objc_sync_exit(self)
        return nil
    }
    
    func removeImage(key:String){
        objc_sync_enter(self)
        if let idx = hashList.index(of: key){
            if let path = paths[key]{
                _ = try? FileManager.default.removeItem(atPath: path)
            }
            hashList.remove(at: idx)
        }
        objc_sync_exit(self)
    }
    
    func check(){
        objc_sync_enter(self)
        while hashList.count > maximum{
            objc_sync_exit(self)
            self.removeImage(key: hashList.last!)
            objc_sync_enter(self)
        }
        objc_sync_exit(self)
    }
    
    func clear(){
        objc_sync_enter(self)
        while hashList.count > 0{
            objc_sync_exit(self)
            self.removeImage(key: hashList.last!)
            objc_sync_enter(self)
        }
        objc_sync_exit(self)
    }
}



class UIImageViewExtensionVariables{
    var indicator = UIActivityIndicatorView()
    var progressIndicator = UIProgressView(progressViewStyle: .bar)
    var defaultImage: UIImage?
    var url: URL? = URL(fileURLWithPath: "/")
}

extension UIImageView{
    static var ExtVariables = [UIImageView:UIImageViewExtensionVariables]()
    var indicator: UIActivityIndicatorView!{
        get{
            if UIImageView.ExtVariables[self] == nil{
                UIImageView.ExtVariables[self] = UIImageViewExtensionVariables()
            }
            return UIImageView.ExtVariables[self]?.indicator
        }
    }
    var progressIndicator: UIProgressView!{
        get{
            if UIImageView.ExtVariables[self] == nil{
                UIImageView.ExtVariables[self] = UIImageViewExtensionVariables()
            }
            return UIImageView.ExtVariables[self]?.progressIndicator
        }
    }
    var defaultImage: UIImage?{
        get{
            if UIImageView.ExtVariables[self] == nil{
                UIImageView.ExtVariables[self] = UIImageViewExtensionVariables()
            }
            return UIImageView.ExtVariables[self]?.defaultImage
        }
        set(img){
            if UIImageView.ExtVariables[self] == nil{
                UIImageView.ExtVariables[self] = UIImageViewExtensionVariables()
            }
            UIImageView.ExtVariables[self]?.defaultImage = img
        }
    }
    
    private var url: URL?{
        get{
            if UIImageView.ExtVariables[self] == nil{
                UIImageView.ExtVariables[self] = UIImageViewExtensionVariables()
            }
            return UIImageView.ExtVariables[self]?.url
        }
        set(url){
            if UIImageView.ExtVariables[self] == nil{
                UIImageView.ExtVariables[self] = UIImageViewExtensionVariables()
            }
            UIImageView.ExtVariables[self]?.url = url
        }
    }
    
    func loadURL(url:URL?, callback:((UIImage?)->Void)? = nil){
        if self.url == url{
            return
        }
        self.clear()
        self.url = url
        if let url = url{
            indicator.center = CGPoint(x: self.frame.width/2, y: self.frame.height/2)
            progressIndicator.center = CGPoint(x: self.frame.width/2, y: self.frame.height/2)
            self.addSubview(indicator)
            self.addSubview(progressIndicator)
            indicator.style = .gray
            indicator.startAnimating()
            progressIndicator.isHidden = true
            DispatchQueue.main.asyncAfter(deadline: .now()+0.1, execute: {
                self.indicator.center = CGPoint(x: self.frame.width/2, y: self.frame.height/2)
            })
            DispatchQueue.global().async {
                let image = ImageManager.shared.imageForURL(url: url, progressCallback: /*{ (progress) in
                    DispatchQueue.main.async {
                        if self.progressIndicator.isHidden{
                            self.progressIndicator.isHidden = false
                            self.indicator.isHidden = true
                        }
                        self.progressIndicator.progress = Float(progress)
                    }
 
                }*/nil)
                DispatchQueue.main.async {
                    if self.url == url{
                        if let image = image{
                            self.image = image
                            callback?(image)
                        }
                        else{
                            self.image = self.defaultImage
                            callback?(self.defaultImage)
                        }
                    }
                    self.indicator.removeFromSuperview()
                    self.progressIndicator.removeFromSuperview()
                }
            }
        }
        else{
            
        }
    }
    func clear(){
        self.url = nil
        self.image = defaultImage
        self.indicator.removeFromSuperview()
        self.progressIndicator.removeFromSuperview()

    }
}
