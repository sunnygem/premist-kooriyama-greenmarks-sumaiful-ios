//
//  Client.swift
//  SUMAIFUL
//
//  Created by occlusion on 8/20/18.
//  Copyright © 2018 Sunny Gem Co., Ltd. All rights reserved.
//

import UIKit
import Alamofire

class Client: ClientBase {
    static let shared = Client()
    
    var termOfUseURL:URL?
    var termOfUseHtml:String?
    var privacyPolicyURL:URL?
    var privacyPolicyHtml:String?

    var informationList = [Information]()
    var accessoryList = [Accessory]()
    var conciergeList = [Concierge]()
    var appList = [LaunchApp]()
    var appList2 = [LaunchApp]()
    
    var isEnglish = false
    let presentationDateFormatter = DateFormatter()

    override init() {
        super.init()
        load()
        #if DEBUG
        endpoint = "https://dev-premist-kooriyama-greenmarks-sumaiful.sgx.jp/"
        EncryptSecret = "dcfe967571a6b5542069c1063ea1b165d3ae2657b9d2a91706ff1ef1653aca28"
        EncryptSharedPassword = "7c8776e3ceb8f646c1fb1b909dc05d58fb2f74e26a4d9bc4947e0be175d4ff39"
        #else //Release
        endpoint = "https://sumaiful.com/"
        EncryptSecret = "dcfe967571a6b5542069c1063ea1b165d3ae2657b9d2a91706ff1ef1653aca28"
        EncryptSharedPassword = "7c8776e3ceb8f646c1fb1b909dc05d58fb2f74e26a4d9bc4947e0be175d4ff39"
        #endif
        
        BasicUser = "dev-premist-kooriyama-greenmarks-sumaiful"
        BasicPassword = "sgx.jp"
        UniqueParameter = "sumaiful"
        
        presentationDateFormatter.dateFormat = "yyyy.MM.dd"
        
        setup()
    }
    
    override func didLogin(){
        self.fetchTermOfUse { (html, url, err) in
            self.termOfUseURL = url
            self.termOfUseHtml = html
        }
        self.fetchPrivacyPolicy { (html, url, err) in
            self.privacyPolicyURL = url
            self.privacyPolicyHtml = html
        }
    }
    override func parseLoginReply(data:[String:Any]?, callback:@escaping (User?, Error?)->Void)->Bool{
        if let user = self.parseUser(data: data){
            self.myUser.copy(user: user)
            self.didLogin()
            self.save()
            callback(self.myUser, nil)
            return true
        }
        return false
    }

    func fetchTermOfUse(callback:@escaping (String?, URL?, Error?)->Void){
        self.call(url: apiUrl + "terms_of_service", parameters: [:]) { (result, error) in
            var html:String?
            var url:URL?
            if let data = result.first{
                if self.isEnglish{
                    html = data["content_en"] as? String
                    url = self.parseUrl(path: data["url_en"])
                }
                else{
                    html = data["content"] as? String
                    url = self.parseUrl(path: data["url"])
                }
            }
            callback(html, url, error)
        }
    }
    func fetchPrivacyPolicy(callback:@escaping (String?, URL?, Error?)->Void){
        self.call(url: apiUrl + "privacy_policy", parameters: [:]) { (result, error) in
            var html:String?
            var url:URL?
            if let data = result.first{
                if self.isEnglish{
                    html = data["content_en"] as? String
                    url = self.parseUrl(path: data["url_en"])
                }
                else{
                    html = data["content"] as? String
                    url = self.parseUrl(path: data["url"])
                }
            }
            callback(html, url, error)
        }
    }
    
    
    func fetchTopInformation(page:Int = 0, callback:@escaping ([Information], Error?)->Void){
        call(url: apiUrl + "top", parameters: ["device_token":self.deviceToken ?? "", "os":"ios", "user_type":"\(myUser.type.rawValue)"]) { (result, error) in
            var list = [Information]()
            if let infoList = result.first?["information"] as? [[String:Any]]{
                for data in infoList{
                    if let info = self.parseInformation(data){
                        list.append(info)
                    }
                }
            }
            callback(list, error)
        }
    }
    
    func fetchLastUsedAccessories(callback:@escaping ([Accessory], NSError?)->Void){
    }
    
    
    func fetchAllAccessories(callback:@escaping ([Accessory], Error?)->Void){
        self.call(url: apiUrl + "homekit", parameters: [:]) { (result, error) in
            var list = [Accessory]()
            for data in result{
                if let accessory = self.parseAccessory(data){
                    list.append(accessory)
                }
            }
            callback(list, error)
        }
    }
    
    func fetchManuals(category:ManualCategory, callback:@escaping ([ConciergeManuals.Manual], Error?)->Void){
        self.call(url:apiUrl + "manuals", parameters: ["contractant_service_menu_id":"41", "category_id":"\(category.id)"]) { (result, error) in
            var list = [ConciergeManuals.Manual]()
            for data in result{
                if let data = self.parseManual(data){
                    list.append(data)
                }
            }
            callback(list, error)
        }
    }
    
    func fetchManualCategories(callback:@escaping ([ManualCategory], Error?)->Void){
        self.call(url:apiUrl + "manuals", parameters: ["contractant_service_menu_id":"41"]) { (result, error) in
            var list = [ManualCategory]()
            for data in result{
                if let data = self.parseManualCategory(data){
                    list.append(data)
                }
            }
            callback(list, error)
        }
    }

    
    func fetchAllConcierge(callback:@escaping ([Concierge], Error?)->Void){
        self.call(url:apiUrl + "concierge", parameters: ["user_type":"\(myUser.type.rawValue)"]) { (result, error) in
            var list = [Concierge]()
            for data in result{
                if let concierge = self.parseConcierge(data){
                    if self.myUser.type != .resident{
                        if concierge.type == .manual{ continue}
                    }
                    list.append(concierge)
                }
                callback(list, error)
            }
        }
    }
    
    

    func fetchAllInformation(callback:@escaping ([MoreSumaiful], Error?)->Void){
        call(url: apiUrl + "more_sumaiful", parameters: [:]) { (list, error) in
            var result = [MoreSumaiful]()
            for data in list{
                if let ms = self.parseMoreSumaiful(data){
                    result.append(ms)
                }
            }
            callback(result, error)
        }
    }
    
    override func parseUser(data: [String : Any]?) -> User? {
        guard let data = data else{return nil}
        let user = User()
        user.userId = data["id"] as? Int ?? 0
        if let front = data["front_user"] as? [String:Any]{
            user.type = User.UserType(rawValue: front["type"] as? Int ?? 0) ?? User.UserType.contract
            user.name = front["nickname"] as? String ?? ""
        }
        return user
    }
    
    
    func fetchAllAppList(callback:@escaping ([LaunchApp], NSError?)->Void){
        DispatchQueue.global().async {
            callback(self.appList, nil)
        }
    }
    
    func fetchAllAppList2(callback:@escaping ([LaunchApp], NSError?)->Void){
        DispatchQueue.global().async {
            callback(self.appList2, nil)
        }
    }
    
    func fetchDataRequest(url:URL)->DataRequest{
        return Alamofire.request(url, method: .get, parameters: nil, headers: basicHeaders)
    }
    
    func fetchIconUrl(appId:String, callback:@escaping ((URL?, Error?)->Void)){
        Alamofire.request(URL(string: "https://itunes.apple.com/jp/lookup?id=\(appId)")!, method: .get, parameters:
            [:], headers:basicHeaders).responseJSON { (response) in
                if let result = response.result.value as? [String:Any]{
                    if let results = result["results"] as? [[String:Any]]{
                        if let r = results.first{
                            if let url = r["artworkUrl512"] as? String{
                                callback(URL(string:url), nil)
                                return
                            }
                            else if let url = r["artworkUrl60"] as? String{
                                callback(URL(string:url), nil)
                                return
                            }
                            else if let url = r["artworkUrl100"] as? String{
                                callback(URL(string:url), nil)
                                return
                            }
                        }
                    }
                }
                callback(nil, response.error)

        }

        
    }
    
}
