//
//  Concierge.swift
//  SUMAIFUL
//
//  Created by occlusion on 8/20/18.
//  Copyright © 2018 Sunny Gem Co., Ltd. All rights reserved.
//

import UIKit


enum ConciergeType: Int {
    case unknown = 0
    case contactPhone = 1
    case contactEmail = 2
    case contactForm = 3
    case linkGroup = 4
    case blog = 5
    case app = 6
    case manual = 7
    case reservation = 8
    case survey = 9
    case chat = 10
    case link = 11

}
class Concierge: NSObject {
    
    var icon:URL?
    var name = ""
    var information = ""
    var type = ConciergeType.unknown
    var data:ConciergeData?
}


class ConciergeData: NSObject{
    
}

class ConciergeContacts: ConciergeData{
    var tel = ""
    var title = ""
    var url: URL?
    var reserveTitle = ""
    var reserveUrl: URL?
    var comment = ""
}

class ConciergeApplications: ConciergeData{
    var apps = [LaunchApp]()
    var memo = ""
}

class ConciergeManuals: ConciergeData{
    class Manual: NSObject{
        var name = ""
        var date = Date()
        var files = [URL]()
    }
    var manuals = [Manual]()
}

class ConciergeSurvey: ConciergeData{
    class Survey: NSObject{
        var id = 0
        var title = ""
    }
    var surveys = [Survey]()
    
}

class ConciergeLinks: ConciergeData{
    var links = [URL]()
}


class ConciergeApp: Concierge {
    
    class Data: NSObject {
        var icon: UIImage?
    }
//    var data = [Data]()
}

class ConciergeAnswerData: NSObject {
    var answer:ConciergeAnswer?
    var isOn = false
}

class ConciergeAnswer: NSObject{
    var title = ""
}

class ConciergeQuestion: Concierge {
    
}




class ManualCategory: NSObject{
    var id = 0
    var icon:URL?
    var title = ""
}
