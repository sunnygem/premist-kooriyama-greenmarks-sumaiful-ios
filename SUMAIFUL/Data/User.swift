//
//  User.swift
//  SUMAIFUL
//
//  Created by occlusion on 2/5/19.
//  Copyright © 2019 Sunny Gem Co., Ltd. All rights reserved.
//

import UIKit

class User: NSObject {
    enum UserType:Int {
        case contract = 0
        case resident = 1
        case review = 2
    }

    var name = ""
    var userId = 11
    var type = UserType.contract
    
    func copy(user:User?){
        guard let src = user else{return}
        name = src.name
        userId = src.userId
        type = src.type
    }
}
