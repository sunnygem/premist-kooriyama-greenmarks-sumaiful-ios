//
//  LaunchAppCollectionViewCell.swift
//  SUMAIFUL
//
//  Created by occlusion on 8/20/18.
//  Copyright © 2018 Sunny Gem Co., Ltd. All rights reserved.
//

import UIKit

class LaunchAppCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var thumbView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var favoriteIcon: UIView!
    var appData:LaunchApp?{
        didSet{
            reload()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        
        thumbView.clipsToBounds = true
        thumbView.layer.cornerRadius = 8
        favoriteIcon.layer.cornerRadius = favoriteIcon.frame.width/2
        favoriteIcon.clipsToBounds = true
        
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(longTap(_:)))
        self.addGestureRecognizer(longPress)
    }
    
    func reload(){
        favoriteIcon.isHidden = !(appData?.isFavorite ?? true)
    }
    
    @objc func longTap(_ sender:UILongPressGestureRecognizer){
        guard let data = appData else{return}
        if sender.state == .began{
            data.isFavorite = !data.isFavorite
            reload()
        }
    }
    
}
