//
//  LaunchAppViewController.swift
//  SUMAIFUL
//
//  Created by occlusion on 8/20/18.
//  Copyright © 2018 Sunny Gem Co., Ltd. All rights reserved.
//

import UIKit
import StoreKit
class LaunchAppViewController: BaseViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, SKStoreProductViewControllerDelegate {

    @IBOutlet weak var memoLabel: UILabel?
    @IBOutlet weak var iconView: UIImageView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var memo = ""{
        didSet{
            memoLabel?.text = memo
        }
    }
    var appList = [LaunchApp](){
        didSet{
            appList.sort { (l1, l2) -> Bool in
                return (l1.isFavorite ? 1:0) > (l2.isFavorite ? 1:0)
            }

        }
    }
    @IBOutlet weak var titleLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = self.title
        memoLabel?.text = memo
    }
    
    
    @IBAction func goBack(_ sender:Any){
        self.rootViewController?.goBack()
    
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return appList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Main", for: indexPath)
        let app = appList[indexPath.row]
        if let c = cell as? LaunchAppCollectionViewCell{
            c.appData = app
            Client.shared.fetchIconUrl(appId: app.appId) { (url, error) in
                c.thumbView.loadURL(url: url)
            }
            c.titleLabel.text = app.name
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let app = appList[indexPath.row]
        /*
        if let url = app.scheme{
            if UIApplication.shared.canOpenURL(url){
                UIApplication.shared.open(url, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
            }
            else{
                let storeProductViewController = SKStoreProductViewController()
                let parameters = [SKStoreProductParameterITunesItemIdentifier: app.appId]
                storeProductViewController.delegate = self
                storeProductViewController.loadProduct(withParameters: parameters) { (success, error) in
                }
                self.present(controller: storeProductViewController)
            }
        }
 */
        let storeProductViewController = SKStoreProductViewController()
        let parameters = [SKStoreProductParameterITunesItemIdentifier: app.appId]
        storeProductViewController.delegate = self
        storeProductViewController.loadProduct(withParameters: parameters) { (success, error) in
        }
        self.present(controller: storeProductViewController)

    }
    
    func productViewControllerDidFinish(_ viewController: SKStoreProductViewController) {
        viewController.dismiss(animated: true, completion: nil)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if UI_USER_INTERFACE_IDIOM() == .pad{
            let w = (collectionView.bounds.width)/5-2
            return CGSize(width: w, height: w)
        }
        else{
            let w = (collectionView.bounds.width)/3-2
            return CGSize(width: w, height: w)
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 2
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 2
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
