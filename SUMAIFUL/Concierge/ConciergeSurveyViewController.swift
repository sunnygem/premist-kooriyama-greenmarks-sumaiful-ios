//
//  ConciergeSurveyViewController.swift
//  SUMAIFUL
//
//  Created by occlusion on 8/20/18.
//  Copyright © 2018 Sunny Gem Co., Ltd. All rights reserved.
//

import UIKit

class ConciergeSurveyViewController: BaseViewController {

    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailTextView: UITextView!
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var okButton: UIButton!
    
    override var useLogo: Bool{
        return false
    }
    
    var surveyNumber = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()

        okButton.clipsToBounds = true
        backButton.clipsToBounds = true
        
        okButton.layer.cornerRadius = okButton.frame.height/2
        backButton.layer.cornerRadius = backButton.frame.height/2
        numberLabel.text = "\(surveyNumber)"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func doBack(){
        self.rootViewController?.goBack()
    }
    
    func doOk(){
        
    }

    @IBAction func didTapBack(_ sender: Any) {
        doBack()
    }
    
    @IBAction func didTapOk(_ sender: Any) {
        doOk()
    }
    
    func goNextSurvey(survey:ConciergeSurveyViewController?){
        if let survey = survey{
            survey.surveyNumber = surveyNumber+1
            
            self.rootViewController?.showViewController(viewController: survey)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
