//
//  ConciergeManualListCell.swift
//  SUMAIFUL
//
//  Created by occlusion on 5/13/19.
//  Copyright © 2019 Sunny Gem Co., Ltd. All rights reserved.
//

import UIKit

class ConciergeManualListCell: UITableViewCell {

    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
