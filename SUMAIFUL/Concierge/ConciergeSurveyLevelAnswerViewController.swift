//
//  ConciergeSurveyLevelAnswerViewController.swift
//  SUMAIFUL
//
//  Created by occlusion on 8/20/18.
//  Copyright © 2018 Sunny Gem Co., Ltd. All rights reserved.
//

import UIKit

//Level Answer
class ConciergeSurveyLevelAnswerViewController: ConciergeSurveyOneAnswerViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        var i = 1
        for ans in answers{
            let a = ConciergeAnswer()
            a.title = "\(i)"
            ans.answer = a
            i += 1
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    override func doOk() {
        self.goNextSurvey(survey: self.storyboard?.instantiateViewController(withIdentifier: "TwoChoice") as? ConciergeSurveyViewController)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
