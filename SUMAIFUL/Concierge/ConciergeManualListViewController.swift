//
//  ConciergeManualListViewController.swift
//  SUMAIFUL
//
//  Created by occlusion on 5/13/19.
//  Copyright © 2019 Sunny Gem Co., Ltd. All rights reserved.
//

import UIKit

class ConciergeManualListViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate {
    

    var manuals = [ConciergeManuals.Manual]()
    var category: ManualCategory?{
        didSet{
            reload()
        }
    }
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        reload()
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = .clear
        cell.contentView.backgroundColor = .clear
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return manuals.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 64
    }
    
    @IBAction func back(_ sender: Any) {
        rootViewController?.goBack()
    }
    
    func reload(){
        guard self.tableView != nil else{return}
        manuals.removeAll()
        if let c = category{
            Client.shared.fetchManuals(category: c) { (manuals, err) in
                self.manuals = manuals
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        }
        else{
            self.tableView.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Main", for: indexPath)
        if let manualCell = cell as? ConciergeManualListCell{
            let data = manuals[indexPath.row]
            manualCell.nameLabel?.text = data.name
            manualCell.dateLabel?.text = Client.shared.presentationDateFormatter.string(from: data.date) + " UP"
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let data = manuals[indexPath.row]
        if let url = data.files.first{
            openURL(url: url, name:data.name)
        }

    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
