//
//  ConciergeManualCategoryViewController.swift
//  SUMAIFUL
//
//  Created by occlusion on 1/25/20.
//  Copyright © 2020 Sunny Gem Co., Ltd. All rights reserved.
//

import UIKit

class ConciergeManualCategoryViewController: BaseViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var collectionView: UICollectionView!
    var categories = [ManualCategory]()
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Main", for: indexPath)
        let accessory = categories[indexPath.row]
        if let c = cell as? AccessoryCollectionViewCell{
            c.nameLabel.text = accessory.title
            c.infoLabel.text = ""
            c.iconView.defaultImage = UIImage(named: "concierge_manual_icon1")
            c.iconView.loadURL(url: accessory.icon)
        }
        return cell
    }
    
    @IBAction func back(_ sender: Any) {
        rootViewController?.goBack()
    }
    
    func reload(){
        
        Client.shared.fetchManualCategories { (categories, err) in
            DispatchQueue.main.async {
                self.categories = categories
                self.collectionView.reloadData()
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        reload()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "ManualList") as? ConciergeManualListViewController{
            vc.category = categories[indexPath.row]
            self.rootViewController?.showViewController(viewController: vc)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if UI_USER_INTERFACE_IDIOM() == .pad{
            let w = (collectionView.bounds.width)/5-2
            return CGSize(width: w, height: w)
        }
        else{
            let w = (collectionView.bounds.width)/3-2
            return CGSize(width: w, height: w)
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 2
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 2
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
