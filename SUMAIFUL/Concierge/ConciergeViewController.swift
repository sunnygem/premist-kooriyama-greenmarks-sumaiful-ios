//
//  ConciergeViewController.swift
//  SUMAIFUL
//
//  Created by occlusion on 8/19/18.
//  Copyright © 2018 Sunny Gem Co., Ltd. All rights reserved.
//

import UIKit

class ConciergeViewController: TabBaseViewController {

    
    
    
    override func setup() {
        self.tabBarItem.selectedImage = UIImage(named: "tab_icon_concierge_on")?.withRenderingMode(.alwaysOriginal)
        self.tabBarItem.image = UIImage(named: "tab_icon_concierge_off")?.withRenderingMode(.alwaysOriginal)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        showViewController(viewController: self.storyboard?.instantiateViewController(withIdentifier: "Root"), animated:false)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
