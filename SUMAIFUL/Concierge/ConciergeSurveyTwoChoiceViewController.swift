//
//  ConciergeSurveyTwoChoiceViewController.swift
//  SUMAIFUL
//
//  Created by occlusion on 8/20/18.
//  Copyright © 2018 Sunny Gem Co., Ltd. All rights reserved.
//

import UIKit

//Two Choice

class ConciergeSurveyTwoChoiceViewController: ConciergeSurveyViewController {

    @IBOutlet weak var noButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        noButton.clipsToBounds = true
        noButton.layer.cornerRadius = noButton.frame.height/2
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func didTapNo(_ sender: Any) {
        self.goNextSurvey(survey: self.storyboard?.instantiateViewController(withIdentifier: "FreeAnswer") as? ConciergeSurveyViewController)
    }
    
    override func doOk() {
        self.goNextSurvey(survey: self.storyboard?.instantiateViewController(withIdentifier: "FreeAnswer") as? ConciergeSurveyViewController)
    }

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
