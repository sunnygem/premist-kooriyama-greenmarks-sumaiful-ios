//
//  ConciergeSurveyMultiAnswerViewController.swift
//  SUMAIFUL
//
//  Created by occlusion on 8/20/18.
//  Copyright © 2018 Sunny Gem Co., Ltd. All rights reserved.
//

import UIKit

//Multiple Answer
class ConciergeSurveyMultiAnswerViewController: ConciergeSurveyViewController, UITableViewDelegate, UITableViewDataSource {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MainCell", for: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let c = tableView.cellForRow(at: indexPath) as? MultiAnswerTableViewCell{
            c.isOn = !c.isOn
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func doOk() {
        self.goNextSurvey(survey: self.storyboard?.instantiateViewController(withIdentifier: "LevelAnswer") as? ConciergeSurveyViewController)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
