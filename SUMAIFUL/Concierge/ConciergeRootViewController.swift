//
//  ConciergeRootViewController.swift
//  SUMAIFUL
//
//  Created by occlusion on 8/20/18.
//  Copyright © 2018 Sunny Gem Co., Ltd. All rights reserved.
//

import UIKit

class ConciergeRootViewController: BaseViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var collectionView: UICollectionView!
    var conciergeList = [Concierge]()
    
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        reload()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func reload(){
        self.collectionView.willLoadData()

        Client.shared.fetchAllConcierge { (concierge, err) in
            DispatchQueue.main.async {
                self.conciergeList = concierge
                self.collectionView.didEndLoadData()
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return conciergeList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Main", for: indexPath)
        let accessory = conciergeList[indexPath.row]
        if let c = cell as? AccessoryCollectionViewCell{
            c.nameLabel.text = accessory.name
            c.infoLabel.text = accessory.information
            c.iconView.loadURL(url: accessory.icon)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let concierge = conciergeList[indexPath.row]
        switch concierge.type {
        case .contactPhone:
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "Contact") as? ConciergeContactViewController{
                vc.data = concierge.data as? ConciergeContacts
                self.rootViewController?.showViewController(viewController: vc)
            }
            break

        case .app:
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "LaunchApp") as? LaunchAppViewController{
                let appData = concierge.data as? ConciergeApplications
                vc.appList = appData?.apps ?? []
                vc.memo = appData?.memo ?? ""
                self.rootViewController?.showViewController(viewController: vc)
                vc.iconView.loadURL(url: concierge.icon)
            }

        case .manual:
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "ManualCategory") as? ConciergeManualCategoryViewController{
                self.rootViewController?.showViewController(viewController: vc)
            }
            break

        case .survey:
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "Web") as? ConciergeSurveyWebViewController{
                self.rootViewController?.showViewController(viewController: vc)
            }
            break
        case .link:
            if let data = concierge.data as? ConciergeLinks{
                if let url = data.links.first{
                    openURL(url: url, name:concierge.name)
                }
            }
            
            break

        default:
            break
        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if UI_USER_INTERFACE_IDIOM() == .pad{
            let w = (collectionView.bounds.width)/5-2
            return CGSize(width: w, height: w)
        }
        else{
            let w = (collectionView.bounds.width)/3-2
            return CGSize(width: w, height: w)
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 2
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 2
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
