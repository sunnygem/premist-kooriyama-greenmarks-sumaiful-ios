//
//  MultiAnswerTableViewCell.swift
//  SUMAIFUL
//
//  Created by occlusion on 8/21/18.
//  Copyright © 2018 Sunny Gem Co., Ltd. All rights reserved.
//

import UIKit

class MultiAnswerTableViewCell: UITableViewCell {

    var isOn = false{
        didSet{
            if isOn{
                checkthumbView.image = UIImage(named: "icon_check_on")
            }
            else{
                checkthumbView.image = UIImage(named: "icon_check_off")
            }
        }
    }
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var checkthumbView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
