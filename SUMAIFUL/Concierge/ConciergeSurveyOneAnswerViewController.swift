//
//  ConciergeSurveyOneAnswerViewController.swift
//  SUMAIFUL
//
//  Created by occlusion on 8/20/18.
//  Copyright © 2018 Sunny Gem Co., Ltd. All rights reserved.
//

import UIKit

//One Answer
class ConciergeSurveyOneAnswerViewController: ConciergeSurveyViewController, UITableViewDataSource, UITableViewDelegate {
    

    
    var answers = [ConciergeAnswerData]()
    var selectedAnswer: ConciergeAnswerData?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        test()
    }
    
    func test(){
        answers.append(ConciergeAnswerData())
        answers.append(ConciergeAnswerData())
        answers.append(ConciergeAnswerData())
        answers.append(ConciergeAnswerData())
        answers.append(ConciergeAnswerData())
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return answers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MainCell", for: indexPath)
        if let c = cell as? OneAnswerTableViewCell{
            let data = answers[indexPath.row]
            c.isOn = data.isOn
            c.titleLabel.text = data.answer?.title ?? "Test"
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let c = tableView.cellForRow(at: indexPath) as? OneAnswerTableViewCell{
            let data = answers[indexPath.row]
            data.isOn = !data.isOn
            c.isOn = data.isOn
            c.data = data
            if c.isOn{
                if let ans = selectedAnswer, selectedAnswer != data{
                    if let idx = answers.index(of: ans){
                        selectedAnswer?.isOn = false
                        tableView.reloadRows(at: [IndexPath(row: idx, section: 0)], with: .automatic)
                    }
                }
                selectedAnswer = data
            }
            else{
                selectedAnswer = nil
            }
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    
    override func doOk() {
        self.goNextSurvey(survey: self.storyboard?.instantiateViewController(withIdentifier: "MultiAnswer") as? ConciergeSurveyViewController)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
