//
//  ConciergeContactViewController.swift
//  SUMAIFUL
//
//  Created by occlusion on 5/13/19.
//  Copyright © 2019 Sunny Gem Co., Ltd. All rights reserved.
//

import UIKit

class ConciergeContactViewController: BaseViewController {

    var data: ConciergeContacts?
    @IBOutlet weak var phoneButton: UIButton!
    @IBOutlet weak var homepageButton: UIButton!
    @IBOutlet weak var reserveButton: UIButton!
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var reserveButtonHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var homepageButtonHeightConstraint: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()

        reserveButton.layer.borderColor = UIColor.lightGray.cgColor
        homepageButton.layer.borderColor = UIColor.lightGray.cgColor
        phoneButton.layer.borderColor = UIColor.lightGray.cgColor
        reserveButton.layer.borderWidth = 1
        homepageButton.layer.borderWidth = 1
        phoneButton.layer.borderWidth = 1

    }
    
    @IBAction func goBack(_ sender: Any) {
        rootViewController?.goBack()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let data = data{
            phoneButton.setTitle(data.tel, for: .normal)
            
            if data.url == nil{
                homepageButton.isHidden = true
                homepageButtonHeightConstraint.constant = 0
            }
            else{
                homepageButton.isHidden = false
                if !data.title.isEmpty{
                    homepageButton.setTitle(data.title, for: .normal)
                }
            }

            if data.reserveUrl == nil{
                reserveButtonHeightConstraint.constant = 0
                reserveButton.isHidden = true
            }
            else{
                reserveButton.isHidden = false
                if !data.reserveTitle.isEmpty{
                    reserveButton.setTitle(data.reserveTitle, for: .normal)
                }
            }
            
            commentLabel.text = data.comment
        }
    }
    
    @IBAction func openReserve(_ sender: Any) {
        if let url = data?.reserveUrl{
            self.openURL(url: url)
        }
    }
    
    @IBAction func openHomepage(_ sender: Any) {
        if let url = data?.url{
            self.openURL(url: url)
        }
    }
    
    @IBAction func openPhone(_ sender: Any) {
        guard let data = data else{return}
        let number = data.tel
        UIApplication.shared.open(URL(string: "tel:\(number)")!)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
