//
//  EncryptionHelper.m
//  willing
//
//  Created by occlusion on 3/31/19.
//  Copyright © 2019 Sunny Gem Co., Ltd. All rights reserved.
//

#import "EncryptionHelper.h"
#include "Encryption.hpp"
#import <CoreFoundation/CoreFoundation.h>

@implementation EncryptionHelper
    +(void)initOpenSSL{
        willing::InitOpenSSL();
    }

    +(NSString *)encrypt:(NSString *)data password:(NSString *)password iv:(NSString *)iv salt:(NSString *)salt info:(NSString *)info{
        auto r = willing::encrypt(data.UTF8String, password.UTF8String, iv.UTF8String, salt.UTF8String);
        return [[NSString alloc] initWithUTF8String:r.c_str()];
    }
    
    +(NSString *)decrypt:(NSString *)data password:(NSString *)password iv:(NSString *)iv salt:(NSString *)salt info:(NSString *)info{
        auto r = willing::decrypt(data.UTF8String, password.UTF8String, iv.UTF8String, salt.UTF8String);
        return [[NSString alloc] initWithUTF8String:r.c_str()];
    }


@end
