//
//  AspectRatioImageView.swift
//  willing
//
//  Created by occlusion on 3/17/19.
//  Copyright © 2019 Sunny Gem Co., Ltd. All rights reserved.
//

import UIKit

class AspectRatioImageView: UIImageView {

    var ratioLayout: NSLayoutConstraint?
    override var image: UIImage?{
        didSet{
            updateRatio()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        updateRatio()
    }

    func updateRatio(){
        guard let image = image else{return}
        if let layout = ratioLayout{
            self.removeConstraint(layout)
        }
        let ratio = image.size.height/image.size.width
        ratioLayout = NSLayoutConstraint(item: self, attribute: .height, relatedBy: .equal, toItem: self, attribute: .width, multiplier: ratio, constant: 0)
        self.addConstraint(ratioLayout!)
        
    }
}


class HeightBasedAspectRatioImageView: UIImageView {
    
    var ratioLayout: NSLayoutConstraint?
    override var image: UIImage?{
        didSet{
            updateRatio()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        updateRatio()
    }
    
    func updateRatio(){
        guard let image = image else{return}
        if let layout = ratioLayout{
            self.removeConstraint(layout)
        }
        let ratio = image.size.width/image.size.height
        ratioLayout = NSLayoutConstraint(item: self, attribute: .width, relatedBy: .equal, toItem: self, attribute: .height, multiplier: ratio, constant: 0)
        self.addConstraint(ratioLayout!)
        
    }
}
