//
//  EncryptionHelper.h
//  willing
//
//  Created by occlusion on 3/31/19.
//  Copyright © 2019 Sunny Gem Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface EncryptionHelper : NSObject

    +(void)initOpenSSL;
+(NSString *)encrypt:(NSString *)data password:(NSString *)password iv:(NSString *)iv salt:(NSString *)salt info:(NSString *)info;
+(NSString *)decrypt:(NSString *)data password:(NSString *)password iv:(NSString *)iv salt:(NSString *)salt info:(NSString *)info;

@end

NS_ASSUME_NONNULL_END
