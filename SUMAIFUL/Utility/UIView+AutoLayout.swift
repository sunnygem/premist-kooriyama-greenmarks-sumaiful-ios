//
//  NSView+AutoLayout.swift
//  RoSS
//
//  Created by occlusion on 4/25/16.
//  Copyright © 2016 mbp. All rights reserved.
//

import UIKit


extension UIView{
    @discardableResult func addRelatedLayout(attribute attr1: NSLayoutConstraint.Attribute, toItem view2: AnyObject?, attribute attr2: NSLayoutConstraint.Attribute, constant c: CGFloat)->NSLayoutConstraint{
        return self.addRelatedLayout(attribute: attr1, relatedBy:NSLayoutConstraint.Relation.equal, toItem:view2, attribute: attr2, constant: c)
    }
    
    
    @discardableResult func addRatioLayout(multiplier:CGFloat = 1.0)->NSLayoutConstraint{
        let layout = NSLayoutConstraint(item:self, attribute:.width, relatedBy:.equal, toItem: self, attribute:.height, multiplier: multiplier, constant:0)
        layout.priority = UILayoutPriority.required
        self.addConstraint(layout)
        return layout
    }
    

    @discardableResult func addHeightRatioLayout(multiplier:CGFloat = 1.0)->NSLayoutConstraint{
        let layout = NSLayoutConstraint(item:self, attribute:.height, relatedBy:.equal, toItem: self, attribute:.width, multiplier: multiplier, constant:0)
        layout.priority = UILayoutPriority.required
        self.addConstraint(layout)
        return layout
    }
    

    @discardableResult func addRelatedLayout(attribute attr1: NSLayoutConstraint.Attribute, relatedBy relation: NSLayoutConstraint.Relation, toItem view2: AnyObject?, attribute attr2: NSLayoutConstraint.Attribute, constant c: CGFloat, multiplier:CGFloat = 1.0, priority:Float = UILayoutPriority.required.rawValue)->NSLayoutConstraint{
        self.translatesAutoresizingMaskIntoConstraints = false
        let layout = NSLayoutConstraint(item:self, attribute:attr1, relatedBy:relation, toItem: view2, attribute:attr2, multiplier: multiplier, constant:c)
        layout.priority = UILayoutPriority(rawValue: priority)
        self.superview!.addConstraint(layout)
        return layout
    }
    
    @discardableResult func addSizeLayout(_ dimension:NSLayoutConstraint.Attribute, size:CGFloat, relation:NSLayoutConstraint.Relation)->NSLayoutConstraint{
        let layout = NSLayoutConstraint(item: self, attribute: dimension, relatedBy: relation, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 0.0, constant: size)
        layout.priority = UILayoutPriority.required
        self.addConstraint(layout)
        return layout
    }
    
    func addFitToSuperViewSideLayout(_ constant:CGFloat = 0){
        _ = self.addFitToSuperViewLeftLayout(constant)
        _ = self.addFitToSuperViewRightLayout(-constant)
    }
    
    func addHorizontalCenterLayout(_ constant:CGFloat = 0){
        _ = self.addRelatedLayout(attribute: .centerX, toItem: self.superview, attribute: .centerX, constant: constant)
    }
    
    func addVerticalCenterLayout(_ constant:CGFloat = 0){
        _ = self.addRelatedLayout(attribute: .centerY, toItem: self.superview, attribute: .centerY, constant: constant)
    }

    func addCenterLayout(_ vertical:CGFloat = 0, horizontal:CGFloat = 0){
        self.translatesAutoresizingMaskIntoConstraints = false
        self.addHorizontalCenterLayout(horizontal)
        self.addVerticalCenterLayout(vertical)
    }
    
    
    @discardableResult func addFitToSuperViewLeftLayout(_ constant:CGFloat = 0)->NSLayoutConstraint{
        return self.addRelatedLayout(attribute: NSLayoutConstraint.Attribute.left, toItem: self.superview, attribute: NSLayoutConstraint.Attribute.left, constant: constant)
    }
    @discardableResult func addFitToSuperViewRightLayout(_ constant:CGFloat = 0)->NSLayoutConstraint{
        return self.addRelatedLayout(attribute: NSLayoutConstraint.Attribute.right, toItem: self.superview, attribute: NSLayoutConstraint.Attribute.right, constant: constant)
    }
    @discardableResult func addFitToSuperViewTopLayout(_ constant:CGFloat = 0)->NSLayoutConstraint{
        return self.addRelatedLayout(attribute: NSLayoutConstraint.Attribute.top, toItem: self.superview, attribute: NSLayoutConstraint.Attribute.top, constant: constant)
    }
    @discardableResult func addFitToSuperViewBottomLayout(_ constant:CGFloat = 0)->NSLayoutConstraint{
        return self.addRelatedLayout(attribute: NSLayoutConstraint.Attribute.bottom, toItem: self.superview, attribute: NSLayoutConstraint.Attribute.bottom, constant: constant)
    }
    
    
    @discardableResult func addFitToSuperViewSafeTopLayout(_ constant:CGFloat = 0)->NSLayoutConstraint{
        if #available(iOS 11.0, *) {
            return self.addRelatedLayout(attribute: NSLayoutConstraint.Attribute.top, toItem: self.superview?.safeAreaLayoutGuide, attribute: NSLayoutConstraint.Attribute.top, constant: constant)
        }
        else{
            return self.addRelatedLayout(attribute: NSLayoutConstraint.Attribute.top, toItem: self.superview, attribute: NSLayoutConstraint.Attribute.top, constant: constant)
        }
    }
    @discardableResult func addFitToSuperViewSafeBottomLayout(_ constant:CGFloat = 0)->NSLayoutConstraint{
        if #available(iOS 11.0, *) {
            return self.addRelatedLayout(attribute: NSLayoutConstraint.Attribute.bottom, toItem: self.superview?.safeAreaLayoutGuide, attribute: NSLayoutConstraint.Attribute.bottom, constant: constant)
        }
        else{
            return self.addRelatedLayout(attribute: NSLayoutConstraint.Attribute.bottom, toItem: self.superview, attribute: NSLayoutConstraint.Attribute.bottom, constant: constant)
        }
    }

    
    func addFitToSuperViewFitLayout(_ left:CGFloat = 0, right:CGFloat = 0, top:CGFloat = 0, bottom:CGFloat = 0, priority:Float = 1000){
        self.translatesAutoresizingMaskIntoConstraints = false
        self.addFitToSuperViewLeftLayout(left).priority = UILayoutPriority(rawValue: priority)
        self.addFitToSuperViewRightLayout(right).priority = UILayoutPriority(rawValue: priority)
        self.addFitToSuperViewTopLayout(top).priority = UILayoutPriority(rawValue: priority)
        self.addFitToSuperViewBottomLayout(bottom).priority = UILayoutPriority(rawValue: priority)
    }
    
    func addFullScreenLayout(_ constant:CGFloat = 0){
        self.translatesAutoresizingMaskIntoConstraints = false
        _ = self.addRelatedLayout(attribute: NSLayoutConstraint.Attribute.top, toItem: self.superview, attribute: NSLayoutConstraint.Attribute.top, constant: constant)
        _ = self.addRelatedLayout(attribute: NSLayoutConstraint.Attribute.left, toItem: self.superview, attribute: NSLayoutConstraint.Attribute.left, constant: constant)
        _ = self.addRelatedLayout(attribute: NSLayoutConstraint.Attribute.right, toItem: self.superview, attribute: NSLayoutConstraint.Attribute.right, constant: constant)
        _ = self.addRelatedLayout(attribute: NSLayoutConstraint.Attribute.bottom, toItem: self.superview, attribute: NSLayoutConstraint.Attribute.bottom, constant: constant)
    }
    
    func addBottomLayout(){
        self.translatesAutoresizingMaskIntoConstraints = false
        _ = self.addRelatedLayout(attribute: NSLayoutConstraint.Attribute.left, toItem: self.superview, attribute: NSLayoutConstraint.Attribute.left, constant: 0)
        _ = self.addRelatedLayout(attribute: NSLayoutConstraint.Attribute.right, toItem: self.superview, attribute: NSLayoutConstraint.Attribute.right, constant: 0)
        _ = self.addRelatedLayout(attribute: NSLayoutConstraint.Attribute.bottom, toItem: self.superview, attribute: NSLayoutConstraint.Attribute.bottom, constant: 0)
    }

    func addTopLayout(){
        self.translatesAutoresizingMaskIntoConstraints = false
        _ = self.addRelatedLayout(attribute: NSLayoutConstraint.Attribute.left, toItem: self.superview, attribute: NSLayoutConstraint.Attribute.left, constant: 0)
        _ = self.addRelatedLayout(attribute: NSLayoutConstraint.Attribute.right, toItem: self.superview, attribute: NSLayoutConstraint.Attribute.right, constant: 0)
        _ = self.addRelatedLayout(attribute: NSLayoutConstraint.Attribute.top, toItem: self.superview, attribute: NSLayoutConstraint.Attribute.top, constant: 0)
    }
    
    func cleanupLayouts(){
        var targets = [NSLayoutConstraint]()
        for constraint in self.constraints{
            if constraint.firstItem === self && constraint.secondItem === nil{
                targets.append(constraint)
            }
        }
        if let constraints = self.superview?.constraints{
            for constraint in constraints{
                if constraint.firstItem === self || constraint.secondItem === self{
                    targets.append(constraint)
                }
            }
        }
        NSLayoutConstraint.deactivate(targets)
    }
    
    
    func addFullScreenSafeLayout(margin:CGFloat = 0){
        self.translatesAutoresizingMaskIntoConstraints = false
        if #available(iOS 11.0, *) {
            _ = self.addRelatedLayout(attribute: NSLayoutConstraint.Attribute.top, toItem: self.superview?.safeAreaLayoutGuide, attribute: NSLayoutConstraint.Attribute.top, constant: margin)
            _ = self.addRelatedLayout(attribute: NSLayoutConstraint.Attribute.left, toItem: self.superview?.safeAreaLayoutGuide, attribute: NSLayoutConstraint.Attribute.left, constant: margin)
            _ = self.addRelatedLayout(attribute: NSLayoutConstraint.Attribute.right, toItem: self.superview?.safeAreaLayoutGuide, attribute: NSLayoutConstraint.Attribute.right, constant: -margin)
            _ = self.addRelatedLayout(attribute: NSLayoutConstraint.Attribute.bottom, toItem: self.superview?.safeAreaLayoutGuide, attribute: NSLayoutConstraint.Attribute.bottom, constant: -margin)
        }
        else{
            _ = self.addRelatedLayout(attribute: NSLayoutConstraint.Attribute.top, toItem: self.superview, attribute: NSLayoutConstraint.Attribute.top, constant: margin)
            _ = self.addRelatedLayout(attribute: NSLayoutConstraint.Attribute.left, toItem: self.superview, attribute: NSLayoutConstraint.Attribute.left, constant: margin)
            _ = self.addRelatedLayout(attribute: NSLayoutConstraint.Attribute.right, toItem: self.superview, attribute: NSLayoutConstraint.Attribute.right, constant: -margin)
            _ = self.addRelatedLayout(attribute: NSLayoutConstraint.Attribute.bottom, toItem: self.superview, attribute: NSLayoutConstraint.Attribute.bottom, constant: -margin)
        }
    }
    
    func addSafeBottomLayout(){
        self.translatesAutoresizingMaskIntoConstraints = false
        if #available(iOS 11.0, *) {
            _ = self.addRelatedLayout(attribute: NSLayoutConstraint.Attribute.left, toItem: self.superview?.safeAreaLayoutGuide, attribute: NSLayoutConstraint.Attribute.left, constant: 0)
            _ = self.addRelatedLayout(attribute: NSLayoutConstraint.Attribute.right, toItem: self.superview?.safeAreaLayoutGuide, attribute: NSLayoutConstraint.Attribute.right, constant: 0)
            _ = self.addRelatedLayout(attribute: NSLayoutConstraint.Attribute.bottom, toItem: self.superview?.safeAreaLayoutGuide, attribute: NSLayoutConstraint.Attribute.bottom, constant: 0)
        } else {
            _ = self.addRelatedLayout(attribute: NSLayoutConstraint.Attribute.left, toItem: self.superview, attribute: NSLayoutConstraint.Attribute.left, constant: 0)
            _ = self.addRelatedLayout(attribute: NSLayoutConstraint.Attribute.right, toItem: self.superview, attribute: NSLayoutConstraint.Attribute.right, constant: 0)
            _ = self.addRelatedLayout(attribute: NSLayoutConstraint.Attribute.bottom, toItem: self.superview, attribute: NSLayoutConstraint.Attribute.bottom, constant: 0)
        }
    }
}
