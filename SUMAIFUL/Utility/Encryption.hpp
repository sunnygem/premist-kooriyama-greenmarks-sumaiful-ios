//
//  Encryption.hpp
//  willing
//
//  Created by occlusion on 3/31/19.
//  Copyright © 2019 Sunny Gem Co., Ltd. All rights reserved.
//

#ifndef Encryption_hpp
#define Encryption_hpp

#include <stdio.h>
#include <string>

namespace willing{
    void InitOpenSSL();
    std::string encrypt(const std::string &data, const std::string &password, const std::string &iv, const std::string &salt, const std::string &info = "");
    std::string decrypt(const std::string &data, const std::string &password, const std::string &iv, const std::string &salt, const std::string &info = "");
}
#endif /* Encryption_hpp */
