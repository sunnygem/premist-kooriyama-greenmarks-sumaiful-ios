//
//  UIViewController+Alert.swift
//  Tokkun
//
//  Created by occlusion on 3/24/18.
//  Copyright © 2018 SUNNY GEM CO., LTD. All rights reserved.
//

import UIKit

import UIKit
import Foundation
import MBProgressHUD

extension UIViewController{
    
    func show(error:Error){
        self.show(error: error as NSError)
    }
    func show(error:NSError){
        DispatchQueue.main.asyncAfter(deadline: .now()+0.1) {
            let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func show(message:String){
        DispatchQueue.main.asyncAfter(deadline: .now()+0.1) {
            let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }

    func showAlert(message:String, title:String = ""){
        DispatchQueue.main.asyncAfter(deadline: .now()+0.1) {
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }

    
    func showProgress(message:String)->MBProgressHUD{
        let progress = MBProgressHUD.showAdded(to: self.view, animated: false)
        progress.label.text = message
        progress.layer.removeAllAnimations()
        return progress
    }
    
    
    func showAlert(title:String = "", message:String, buttons:[String], callback:((Int)->Void)?){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        var idx = 0
        for button in buttons{
            let i = idx
            alert.addAction(UIAlertAction(title: button, style: button == NSLocalizedString("Cancel", comment: "") ? .cancel : .default, handler: { (action) in
                callback?(i)
            }))
            idx += 1
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func present(controller:UIViewController){
        var root = self
        while (true){
            if let next = root.presentedViewController{
                root = next
            }
            else{
                break
            }
        }
        if controller.modalPresentationStyle == .overCurrentContext{
            root.definesPresentationContext = true
        }
        root.present(controller, animated: true, completion: nil)
    }
}


extension MBProgressHUD{
    func success(){
        self.customView = UIImageView(image: UIImage(named:"Checkmark"))
        self.label.text = NSLocalizedString("Done", comment: "")
        self.mode = .customView
        self.minSize = CGSize(width:120, height:120)
        self.hide(animated: true, afterDelay: 1.0)
    }
    func fail(){
        self.customView = UIImageView(image: UIImage(named:"icon_alert_error"))
        self.label.text = NSLocalizedString("Error", comment: "")
        self.mode = .customView
        self.minSize = CGSize(width:120, height:120)
        self.hide(animated: true, afterDelay: 1.0)
    }
}

