//
//  MessageDialogViewController.swift
//  SUMAIFUL
//
//  Created by occlusion on 4/20/19.
//  Copyright © 2019 Sunny Gem Co., Ltd. All rights reserved.
//

import UIKit

class MessageDialogViewController: UIViewController {

    var didClose:(()->Void)?
    @IBOutlet weak var messageLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        modalTransitionStyle = .crossDissolve
        modalPresentationStyle = .overCurrentContext
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTap(_:))))
        
    }
    
    @objc func didTap(_ sender:Any){
        
        dismiss(animated: true) {
            self.didClose?()
        }
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
