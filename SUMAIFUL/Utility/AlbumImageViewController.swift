//
//  AlbumImageViewController.swift
//  willing
//
//  Created by occlusion on 4/7/19.
//  Copyright © 2019 Sunny Gem Co., Ltd. All rights reserved.
//

import UIKit

class AlbumImageViewController: UIViewController, UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    
    class AlbumImagePageViewController: UIViewController, UIScrollViewDelegate{
        var page = 0
        var scrollView = UIScrollView()
        var imageView = UIImageView()
        var image: UIImage!{
            didSet{
                imageView.image = image
            }
        }
        override func viewDidLoad() {
            super.viewDidLoad()
            scrollView.addSubview(imageView)
            self.view.addSubview(scrollView)
            scrollView.addFullScreenLayout()
            scrollView.maximumZoomScale = 5.0
            scrollView.minimumZoomScale = 1.0
            scrollView.zoomScale = 1.0
            scrollView.contentSize = self.view.bounds.size
            imageView.frame = self.view.bounds
            imageView.contentMode = .scaleAspectFit
            scrollView.delegate = self
            
            
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(didDoubleTap(_ :)))
            tapGesture.numberOfTapsRequired = 2
            scrollView.addGestureRecognizer(tapGesture)
        }
        
        @objc func didDoubleTap(_ sender:UIGestureRecognizer){
            if(scrollView.zoomScale > 1.0){
                scrollView.setZoomScale(1.0, animated: true)
            }
            else{
                let point = sender.location(in: self.imageView)
                scrollView.zoom(to: CGRect(x: point.x, y: point.y, width: 1, height: 1), animated: true)
            }
        }
        
        override func viewDidLayoutSubviews() {
            super.viewDidLayoutSubviews()
            if let size = imageView.image?.size {
                let wrate = scrollView.frame.width / size.width
                let hrate = scrollView.frame.height / size.height
                let rate = min(wrate, hrate, 1)
                imageView.frame.size = CGSize(width:size.width * rate, height:size.height * rate)
                
                scrollView.contentSize = imageView.frame.size
                updateScrollInset()
            }
        }
        
        private func updateScrollInset() {
            scrollView.contentInset = UIEdgeInsets(
                top: max((scrollView.frame.height - imageView.frame.height)/2, 0),
                left: max((scrollView.frame.width - imageView.frame.width)/2, 0),
                bottom: 0,
                right: 0
            );
        }
        
        
        func scrollViewDidZoom(_ scrollView: UIScrollView) {
            updateScrollInset()
        }
        
        func viewForZooming(in scrollView: UIScrollView) -> UIView? {
            return imageView
        }

    }
    var enableReport = true
    var enableDownload = true
    

    let menuButton = UIButton(type: .custom)
    let closeButton = UIButton(type: .custom)
    let pageViewController = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
    
    var imageFiles = [URL](){
        didSet{
            reload()
        }
    }
    var page = 0{
        didSet{
            reload()
        }
    }
    
    
    func didZoom(){
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        closeButton.setImage(UIImage(named: "common_close_wh"), for: .normal)
        menuButton.setImage(UIImage(named: "icon_album_menu_white"), for: .normal)
        self.view.backgroundColor = .black
        pageViewController.delegate = self
        pageViewController.dataSource = self
        self.view.addSubview(pageViewController.view)
        pageViewController.view.addFullScreenLayout()
        
        self.view.addSubview(closeButton)
        closeButton.addFitToSuperViewSafeTopLayout(20)
        closeButton.addFitToSuperViewRightLayout(-20)
        
        self.view.addSubview(menuButton)
        menuButton.addFitToSuperViewSafeBottomLayout(-20)
        menuButton.addFitToSuperViewRightLayout(-20)

        menuButton.addTarget(self, action: #selector(showMenu(_:)), for: .touchUpInside)
        closeButton.addTarget(self, action: #selector(close(_:)), for: .touchUpInside)

    }
    @objc func close(_ sender:Any){
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func reportPrivacy(){
        let image = imageFiles[page]
    }
    
    func reportOther(){
        let image = imageFiles[page]
    }

    func download(){
        let progress = self.showProgress(message: NSLocalizedString("Downloading...", comment: ""))
    }
    
    @objc func showMenu(_ sender:UIButton){
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    
    func reload(){
        if let vc = createViewController(page: self.page){
            pageViewController.setViewControllers([vc], direction: .forward, animated: false, completion: nil)
        }
    }
    
    func createViewController(page:Int)->AlbumImagePageViewController?{
        if page < 0 || page >= imageFiles.count ?? 0{
            return nil
        }
        let vc = AlbumImagePageViewController()
        vc.page = page
        vc.imageView.loadURL(url: imageFiles[page], callback: { img in
            vc.viewDidLayoutSubviews()
        })
        return vc
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        if let currentViewController = pageViewController.viewControllers?.first as? AlbumImagePageViewController{
            return createViewController(page: currentViewController.page-1)
        }
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        if let currentViewController = pageViewController.viewControllers?.first as? AlbumImagePageViewController{
            return createViewController(page: currentViewController.page+1)
        }
        return nil
    }

    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
/*
class ZoomImageViewController : AlbumImageViewController, ZoomTransitionProtocol{
    func viewForTransition() -> UIView {
        let vc = pageViewController.viewControllers?.first as! AlbumImagePageViewController
        return vc.scrollView
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func close(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    /*
    override func didZoom() {
        super.didZoom()
        if let zoom = self.transitioningDelegate as? ZoomTransition{
            if let vc = self.pageViewController.viewControllers?.first as? AlbumImagePageViewController{
                vc.scrollView.zoomScale
            }
        }
    }
    */
    
}

 */
