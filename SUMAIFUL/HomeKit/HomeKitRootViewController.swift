//
//  HomeKitRootViewController.swift
//  SUMAIFUL
//
//  Created by occlusion on 8/20/18.
//  Copyright © 2018 Sunny Gem Co., Ltd. All rights reserved.
//

import UIKit

class HomeKitRootViewController: BaseViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {


    @IBOutlet weak var collectionView: UICollectionView!
    var accessories = [Accessory]()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        reload()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func reload(){
        self.collectionView.willLoadData()
        Client.shared.fetchAllAccessories { (acc, err) in
            DispatchQueue.main.async {
                self.accessories = acc
                self.collectionView.didEndLoadData()
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return accessories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Main", for: indexPath)
        let accessory = accessories[indexPath.row]
        if let c = cell as? AccessoryCollectionViewCell{
            c.nameLabel.text = accessory.name
            c.infoLabel.text = accessory.information
            c.iconView.loadURL(url: accessory.icon)
        }
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let accessory = accessories[indexPath.row]
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "LaunchApp") as? LaunchAppViewController{
            vc.title = accessory.name
            vc.memo = accessory.memo
            vc.appList = accessory.appList
            self.rootViewController?.showViewController(viewController: vc)
            vc.iconView.loadURL(url: accessory.icon)
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if UI_USER_INTERFACE_IDIOM() == .pad{
            let w = (collectionView.bounds.width)/5-2
            return CGSize(width: w, height: w)
        }
        else{
            let w = (collectionView.bounds.width)/3-2
            return CGSize(width: w, height: w)
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 2
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 2
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
