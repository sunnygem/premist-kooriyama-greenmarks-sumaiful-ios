//
//  HomeKitViewController.swift
//  SUMAIFUL
//
//  Created by occlusion on 8/19/18.
//  Copyright © 2018 Sunny Gem Co., Ltd. All rights reserved.
//

import UIKit




class HomeKitViewController: TabBaseViewController {
    
    
    var warningViewController: MessageDialogViewController?
    override func setup() {
        self.tabBarItem.selectedImage = UIImage(named: "tab_icon_homekit_on")?.withRenderingMode(.alwaysOriginal)
        self.tabBarItem.image = UIImage(named: "tab_icon_homekit_off")?.withRenderingMode(.alwaysOriginal)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard Client.shared.myUser.type == .resident else{return}
        showViewController(viewController: self.storyboard?.instantiateViewController(withIdentifier: "Root"), animated:false)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func showWarning(){
        guard warningViewController == nil else{return}
        if let vc = self.tabBarController?.storyboard?.instantiateViewController(withIdentifier: "MessageDialog") as? MessageDialogViewController{
            warningViewController = vc
            vc.loadViewIfNeeded()
            vc.messageLabel.text = NSLocalizedString("HomekitWarningMessage", comment: "")
            self.view.addSubview(vc.view)
            self.addChild(vc)
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if(Client.shared.myUser.type != .resident){
            showWarning()
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
