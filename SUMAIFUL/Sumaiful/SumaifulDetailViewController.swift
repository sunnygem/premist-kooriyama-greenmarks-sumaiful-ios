//
//  CommunicationDetailViewController.swift
//  SUMAIFUL
//
//  Created by occlusion on 8/20/18.
//  Copyright © 2018 Sunny Gem Co., Ltd. All rights reserved.
//

import UIKit
import WebKit
class SumaifulDetailViewController: BaseViewController {

    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contents: WKWebView!
    var data:MoreSumaiful?
    
    override var useLogo: Bool{
        return false
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        dateLabel.text = data?.dateText ?? ""
        titleLabel.text = data?.text ?? ""
        contents.loadHTMLString(data?.contents ?? "", baseURL: nil)
//        detailTextView.text = NSLocalizedString("TestCommunicationText", comment: "")
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func goBack(_ sender: Any) {
        self.rootViewController?.goBack()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
