//
//  SumaifulRootViewController.swift
//  SUMAIFUL
//
//  Created by occlusion on 8/20/18.
//  Copyright © 2018 Sunny Gem Co., Ltd. All rights reserved.
//

import UIKit
import ESPullToRefresh

class SumaifulRootViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {

    var informations = [MoreSumaiful]()
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.estimatedRowHeight = 200
        self.tableView.es.addPullToRefresh {[unowned self] in
            self.reloadData {
                self.tableView.es.stopPullToRefresh()
            }
        }

        reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func reloadData(_ callback:(()->Void)? = nil){

        self.tableView.willLoadData()
        Client.shared.fetchAllInformation { (info, err) in
            DispatchQueue.main.async {
                self.informations = info
                self.tableView.didEndLoadData()
                callback?()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = UIColor.clear
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return informations.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MainCell", for: indexPath)
        let info = informations[indexPath.row]
        if let c = cell as? TopInformationTableViewCell{
            c.dateLabel.text = info.dateText
            c.infoLabel.text = info.text
            c.thumbView.loadURL(url: info.images.first?.url)
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return UITableView.automaticDimension
        return 200
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let info = informations[indexPath.row]
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "Detail") as? SumaifulDetailViewController{
            vc.data = info
            self.rootViewController?.showViewController(viewController: vc)
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
