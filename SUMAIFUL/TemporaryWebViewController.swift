//
//  TemporaryWebViewController.swift
//  SUMAIFUL
//
//  Created by occlusion on 11/20/18.
//  Copyright © 2018 Sunny Gem Co., Ltd. All rights reserved.
//

import UIKit
import WebKit
class TemporaryWebViewController: UIViewController, UITabBarDelegate, WKUIDelegate, WKNavigationDelegate {

    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var tabBar: UITabBar!
    override func viewDidLoad() {
        super.viewDidLoad()
        WKWebsiteDataStore.default().removeData(ofTypes: WKWebsiteDataStore.allWebsiteDataTypes(), modifiedSince: Date(timeIntervalSince1970: 0), completionHandler: {})

        webView.uiDelegate = self
        webView.navigationDelegate = self
        // Do any additional setup after loading the view.
        tabBar.items = [
            UITabBarItem(title: "", image: UIImage(named: "tab_icon_home_off")?.withRenderingMode(.alwaysOriginal),
                         selectedImage: UIImage(named: "tab_icon_home_on")?.withRenderingMode(.alwaysOriginal)),
            UITabBarItem(title: "", image: UIImage(named: "tab_icon_homekit_off")?.withRenderingMode(.alwaysOriginal),
                         selectedImage: UIImage(named: "tab_icon_homekit_on")?.withRenderingMode(.alwaysOriginal)),
            UITabBarItem(title: "", image: UIImage(named: "tab_icon_concierge_off")?.withRenderingMode(.alwaysOriginal),
                         selectedImage: UIImage(named: "tab_icon_concierge_on")?.withRenderingMode(.alwaysOriginal)),
            UITabBarItem(title: "", image: UIImage(named: "tab_icon_smaiful_off")?.withRenderingMode(.alwaysOriginal),
                         selectedImage: UIImage(named: "tab_icon_smaiful_on")?.withRenderingMode(.alwaysOriginal)),
            UITabBarItem(title: "", image: UIImage(named: "tab_icon_communication_off")?.withRenderingMode(.alwaysOriginal),
                         selectedImage: UIImage(named: "tab_icon_communication_on")?.withRenderingMode(.alwaysOriginal)),
        ]
        
        let offset = 8 as CGFloat
        for item in tabBar.items ?? []{
            item.imageInsets = UIEdgeInsets(top: offset, left: 0, bottom: -offset, right: 0)
        }
        if let item = tabBar.items?.first{
            tabBar.selectedItem = item
            self.tabBar(tabBar, didSelect: item)
        }
    }
    
    func webView(_ webView: WKWebView, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        completionHandler(.useCredential, URLCredential(user: "dev-sumaiful", password: "sgx.jp", persistence: .permanent))
    }

    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        let index = tabBar.items?.index(of: item) ?? 0
        let urls = [
            "/",
            "/home-kits",
            "/concierges",
            "/more-sumaifuls",
            "/communications",
            ]
        let root = "https://dev-sumaiful.sgx.jp"
        webView.load(URLRequest(url: URL(string:root+urls[index])!))
    }

    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        guard let url = navigationAction.request.url else {
            decisionHandler(.cancel)
            return
        }
        if !url.absoluteString.hasPrefix("http://")
            && !url.absoluteString.hasPrefix("https://") {
            if UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.openURL(url)
                decisionHandler(.cancel)
                return
            }
        }
        decisionHandler(.allow)
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
