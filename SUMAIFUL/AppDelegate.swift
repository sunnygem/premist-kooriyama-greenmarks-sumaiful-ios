//
//  AppDelegate.swift
//  SUMAIFUL
//
//  Created by occlusion on 8/15/18.
//  Copyright © 2018 Sunny Gem Co., Ltd. All rights reserved.
//

import UIKit
import UserNotifications


extension UITableView{
    func setLoading(flg:Bool){
        var indicator = self.viewWithTag(1212) as? UIActivityIndicatorView
        if indicator == nil{
            let activityIndicator = UIActivityIndicatorView(style: .gray)
            activityIndicator.hidesWhenStopped = true
            activityIndicator.tag = 1212
            self.addSubview(activityIndicator)
            activityIndicator.addCenterLayout()
            indicator = activityIndicator
        }
        if flg{
            indicator?.startAnimating()
        }
        else{
            indicator?.stopAnimating()
        }
    }
    func willLoadData(){
        setLoading(flg: true)
    }
    func didEndLoadData(){
        setLoading(flg: false)
        self.reloadSections(IndexSet(integer: 0), with: .automatic)
    }
}

extension UICollectionView{
    func setLoading(flg:Bool){
        var indicator = self.viewWithTag(1212) as? UIActivityIndicatorView
        if indicator == nil{
            let activityIndicator = UIActivityIndicatorView(style: .gray)
            activityIndicator.hidesWhenStopped = true
            activityIndicator.tag = 1212
            self.addSubview(activityIndicator)
            activityIndicator.addCenterLayout()
            indicator = activityIndicator
        }
        if flg{
            indicator?.startAnimating()
        }
        else{
            indicator?.stopAnimating()
        }
    }
    func willLoadData(){
        setLoading(flg: true)
    }
    func didEndLoadData(){
        setLoading(flg: false)
        self.reloadSections(IndexSet(integer: 0))
    }
}


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        
        Client.shared.load()
        
        EncryptionHelper.initOpenSSL()
        
        Client.shared.isHeldLogin = true
        UNUserNotificationCenter.current().delegate = self
        UNUserNotificationCenter.current().requestAuthorization(options: [.badge, .sound, .alert]) { (granted, err) in
            DispatchQueue.main.async {
                if granted{
                    application.registerForRemoteNotifications()
                }
                else{
                    Client.shared.isHeldLogin = false
                }
            }
        }
        
        // Override point for customization after application launch.
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor:UIColor.black], for: .selected)
        return true
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let deviceTokenString: String = deviceToken.map { String(format: "%.2hhx", $0) }.joined()
        Client.shared.deviceToken = deviceTokenString
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        UIApplication.shared.applicationIconBadgeNumber = 0
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

