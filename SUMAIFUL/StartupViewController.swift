//
//  StartupViewController.swift
//  SUMAIFUL
//
//  Created by occlusion on 8/16/18.
//  Copyright © 2018 Sunny Gem Co., Ltd. All rights reserved.
//

import UIKit

class StartupViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        Client.shared.relogin { (success, error) in
            DispatchQueue.main.async {
                if success{
                    self.performSegue(withIdentifier: "Main", sender: nil)
                }
                else{
                    self.performSegue(withIdentifier: "Login", sender: nil)
                }
            }

        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
